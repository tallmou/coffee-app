package iot.urouen.coffeeIoT.models;


import javax.persistence.*;


@Entity
public class Product {
	@Id
	private long id;

	public Product() {}
	public Product(long id, String name, double quantity, double price) {
		super();
		this.id = id;
		this.name = name;
		this.quantity = quantity;
		this.price=price;
	}

	private String name;

	private double quantity;

	private double price;


	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getQuantity() {
		return quantity;
	}

	public void setQuantity(double quantity) {
		this.quantity = quantity;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

}
