package iot.urouen.coffeeIoT.services;

import iot.urouen.coffeeIoT.models.ConsumeDTO;
import iot.urouen.coffeeIoT.models.Product;

import java.util.List;

public interface ProductService {

    Product save(Product product) throws Exception;
    
    Product update(Product product) throws Exception;

    List<Product> findAll();

    void delete(long id);


    Product findById(Long id);
    
    Product findByName(String name);

    boolean consume(ConsumeDTO consumeDTO);
    
}
