package iot.urouen.coffeeIoT.repositories;

import java.util.Optional;

import org.springframework.data.repository.PagingAndSortingRepository;

import iot.urouen.coffeeIoT.models.Product;

public interface ProductRepository extends PagingAndSortingRepository<Product, Long> {

    Optional<Product> findById(long id);

    Product findByName(String name);

}