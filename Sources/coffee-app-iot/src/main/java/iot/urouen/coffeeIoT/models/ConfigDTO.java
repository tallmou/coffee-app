package iot.urouen.coffeeIoT.models;



public class ConfigDTO {

	private String port;
	private String url;
	private int frequency;
	private String version;


	public String getPort() {
		return port;
	}

	public void setPort(String serverPort) {
		this.port = serverPort;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String serverUrl) {
		this.url = serverUrl;
	}

	public int getFrequency() {
		return frequency;
	}

	public void setFrequency(int frequency) {
		this.frequency = frequency;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

}
