package iot.urouen.coffeeIoT.models;


import java.util.List;

public class MachineStateDTO {
	private String machineId;
	private List<Product> productLog;

	public MachineStateDTO(){}
	public MachineStateDTO(String machineId, List<Product> productLog) {
		this.machineId = machineId;
		this.productLog = productLog;
	}

	public String getMachineId() {
		return machineId;
	}

	public void setMachineId(String machineId) {
		this.machineId = machineId;
	}

	public List<Product> getProductLog() {
		return productLog;
	}

	public void setProductLog(List<Product> productLog) {
		this.productLog = productLog;
	}
}
