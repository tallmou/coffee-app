package iot.urouen.coffeeIoT;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.scheduling.annotation.EnableScheduling;

import iot.urouen.coffeeIoT.configs.ConfigProperties;

@SpringBootApplication
@EnableScheduling
@EnableConfigurationProperties(ConfigProperties.class)
public class CoffeeIoTApplication {
	public static void main(String[] args) {
		SpringApplication.run(CoffeeIoTApplication.class, args);
	}
}
