package iot.urouen.coffeeIoT.services;


import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import iot.urouen.coffeeIoT.models.ConsumeDTO;
import iot.urouen.coffeeIoT.models.Product;
import iot.urouen.coffeeIoT.repositories.ProductRepository;

@Service(value = "productService")
public class ProductServiceImpl implements ProductService {
	
	@Autowired
	private ProductRepository productRepository;
	
	@Override
	public Product save(Product product) throws Exception {
		
		return productRepository.save(product); 
	}

	@Override
	public Product update(Product product) throws Exception {
		
    	Product newProduct = findById(product.getId());
    	if (newProduct != null) {
    		
    		newProduct.setName(product.getName());
    		newProduct.setPrice(product.getPrice());
    		newProduct.setQuantity(product.getQuantity());
    		return productRepository.save(product);
    		 
    	} else {
    		throw new Exception("Produit  introuvable.");
    	}
	}

	@Override
	public List<Product> findAll() {
		List<Product> list = new ArrayList<>();
		productRepository.findAll().iterator().forEachRemaining(list::add);
		return list;
	}

	@Override
	public void delete(long id) {
		productRepository.deleteById(id);
	}

	@Override
	public Product findById(Long id) {
		// TODO Auto-generated method stub
		return productRepository.findById(id).get();
	}



	@Override
	public Product findByName(String name) {
		return productRepository.findByName(name);
	}

	@Override
	public boolean consume(ConsumeDTO consumeDTO) {
		Product newProduct =  findById(consumeDTO.getId());
		if(newProduct != null){
			double newQuatity = newProduct.getQuantity() - consumeDTO.getQuantity();
			newProduct.setQuantity(newQuatity >= 0 ? newQuatity:0);
			productRepository.save(newProduct);
			return true;
		}
		return false;

	}
}
