package iot.urouen.coffeeIoT.services;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import iot.urouen.coffeeIoT.models.Product;
import iot.urouen.coffeeIoT.models.SynchronizationDTO;

@Service
public class RestService {

	@Autowired
	ProductService productService;

	private final RestTemplate restTemplate;

	public RestService(RestTemplateBuilder restTemplateBuilder) {
		this.restTemplate = restTemplateBuilder.build();
	}

	public ResponseEntity<String> synchronize(long machineId, String url ) {
		List<Product> products = productService.findAll();
		//get data
		HttpHeaders headers = new HttpHeaders();
		// set `accept` header
		headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));

		headers.set("x-request-source", "iot");

		HttpEntity<SynchronizationDTO> entityToSend = new HttpEntity<>(new SynchronizationDTO(machineId, products) ,headers);

		//send data
		return this.restTemplate.postForEntity(url,entityToSend,String.class);

	}
}
