package iot.urouen.coffeeIoT.components;

import iot.urouen.coffeeIoT.configs.ConfigProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.server.ConfigurableWebServerFactory;
import org.springframework.boot.web.server.WebServerFactoryCustomizer;
import org.springframework.stereotype.Component;


@Component
public class ServerConfigCustomizer implements WebServerFactoryCustomizer<ConfigurableWebServerFactory> {
    @Autowired
    private ConfigProperties configProperties;

    ConfigurableWebServerFactory factory;
    @Override
    public void customize(ConfigurableWebServerFactory factory) {
        this.factory = factory;
        this.factory.setPort(Integer.parseInt(configProperties.getAgentPort()));
    }

    public void changePort(int port){
        this.factory.setPort(port);
    }


}
