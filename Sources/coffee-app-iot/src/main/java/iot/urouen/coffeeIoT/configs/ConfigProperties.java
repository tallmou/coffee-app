package iot.urouen.coffeeIoT.configs;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "iot")
public class ConfigProperties {

    private String agentPort;
    private String serverPort;
    private String serverUrl;
    private int frequency;
    private long machine;
    private String version;

    public String getAgentPort() {
        return agentPort;
    }

    public void setAgentPort(String agentPost) {
        this.agentPort = agentPost;
    }

    public String getServerPort() {
        return serverPort;
    }

    public void setServerPort(String serverPort) {
        this.serverPort = serverPort;
    }

    public String getServerUrl() {
        return serverUrl;
    }

    public void setServerUrl(String serverUrl) {
        this.serverUrl = serverUrl;
    }

    public int getFrequency() {
        return frequency;
    }

    public void setFrequency(int frequency) {
        this.frequency = frequency;
    }

    public long getMachine() {
        return machine;
    }

    public void setMachine(long machine) {
        this.machine = machine;
    }

    @Override
    public String toString() {
        return "ConfigProperties{" +
                "agentPort='" + agentPort + '\'' +
                ", serverPort='" + serverPort + '\'' +
                ", serverUrl='" + serverUrl + '\'' +
                ", frequency=" + frequency +
                ", machine=" + machine +
                '}';
    }

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}
}