package iot.urouen.coffeeIoT.controllers;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/api")
public class MainController {
	@Value("${iot.version}")
    private String version;

    @GetMapping(path="/version", produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<Object> getVersion(){
        return new ResponseEntity<Object>(version, HttpStatus.OK);
    }
}
