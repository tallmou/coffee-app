package iot.urouen.coffeeIoT.models;

import java.util.List;

public class SynchronizationDTO {
    private long machineId;
    private List<Product> products;


    public SynchronizationDTO(){}

    public SynchronizationDTO(long machineId, List<Product> products) {
        this.machineId = machineId;
        this.products = products;
    }

    public long getMachineId() {
        return machineId;
    }

    public void setMachineId(long machineId) {
        this.machineId = machineId;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }
}
