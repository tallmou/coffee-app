package iot.urouen.coffeeIoT.controllers;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;

import iot.urouen.coffeeIoT.configs.ConfigProperties;
import iot.urouen.coffeeIoT.models.ApiResponse;
import iot.urouen.coffeeIoT.models.ConfigDTO;

@RestController
@RequestMapping(path = "/api/config")
public class ConfigController {
    @Autowired
    private ConfigProperties configProperties;

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<Object> getVersion() throws JsonProcessingException {
        JSONObject config = new JSONObject();
        config.append("agentPort",this.configProperties.getAgentPort());
        config.append("serverPort",this.configProperties.getServerPort());
        config.append("frequency",this.configProperties.getFrequency());
        config.append("serverUrl",this.configProperties.getServerUrl());
        config.append("machine",this.configProperties.getMachine());

        return new ResponseEntity<Object>(config.toString(), HttpStatus.OK);
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> createProduct(@RequestBody ConfigDTO configDTO) throws JsonProcessingException {

        try {
            if(configDTO.getPort() != null) configProperties.setServerPort(configDTO.getPort());
            if(configDTO.getUrl() != null) configProperties.setServerUrl(configDTO.getUrl());
            if(configDTO.getFrequency() != 0) configProperties.setFrequency(configDTO.getFrequency());
            if(configDTO.getVersion() != null) configProperties.setVersion(configDTO.getVersion());
            return new ResponseEntity<Object>(new ApiResponse(HttpStatus.OK,
                    "La configuration a été mise à jour"),HttpStatus.OK);

        } catch(Exception e) {
            return new ResponseEntity<Object>(new ApiResponse(HttpStatus.BAD_REQUEST,
                    "Erreur lors de la mise à jour de la configuration: " + e.getMessage()),HttpStatus.OK);
        }
    }
}
