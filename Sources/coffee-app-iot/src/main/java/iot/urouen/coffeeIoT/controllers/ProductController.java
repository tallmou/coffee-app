package iot.urouen.coffeeIoT.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;

import iot.urouen.coffeeIoT.models.ApiResponse;
import iot.urouen.coffeeIoT.models.ConsumeDTO;
import iot.urouen.coffeeIoT.models.Product;
import iot.urouen.coffeeIoT.models.ProductDTO;
import iot.urouen.coffeeIoT.services.ProductServiceImpl;

@RestController
@RequestMapping(path = "/api/products")
public class ProductController {

    @Autowired
    private ProductServiceImpl productService;



    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
        ResponseEntity<Object> getProducts() throws JsonProcessingException {
            return new ResponseEntity<Object>(productService.findAll(), HttpStatus.OK);
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> createProduct(@RequestBody ProductDTO productDTO) throws JsonProcessingException {

        try {
            Product product = new Product (productDTO.getId(), productDTO.getName(),productDTO.getQuantity(),productDTO.getPrice());

            product = productService.save(product);
            return new ResponseEntity<Object>(new ApiResponse(HttpStatus.OK,
                    "Produit Ajouté à la machine. IdProduit :" + product.getId() +", IdMachine :"  + "null"),HttpStatus.OK);

        }
        catch(Exception e) {
            return new ResponseEntity<Object>(new ApiResponse(HttpStatus.BAD_REQUEST,
                    "Erreur lors de l'ajout du produit : " + e.getMessage()),HttpStatus.OK);
        }
    }
    
    @PutMapping(value = "/{id}",produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> updateProduct(@RequestBody ProductDTO productDTO, @PathVariable long id) throws JsonProcessingException {
    	
        try {
        	System.out.println("id =" + id);
            Product product = new Product (id, productDTO.getName(),productDTO.getQuantity(),productDTO.getPrice());

            product = productService.update(product);
            return new ResponseEntity<Object>(new ApiResponse(HttpStatus.OK,
                    "Produit Modifié à la machine. IdProduit :" + product.getId() +", IdMachine :"  + "null"),HttpStatus.OK);

        }
        catch(Exception e) {
            return new ResponseEntity<Object>(new ApiResponse(HttpStatus.BAD_REQUEST,
                    "Erreur lors de la modification du produit : " + e.getMessage()),HttpStatus.OK);
        }
    }


    @DeleteMapping("/{id}")
    public ResponseEntity<Object> deleteProduct(@PathVariable Long id) throws JsonProcessingException {

        try{
            productService.delete(id);
            return new ResponseEntity<Object>(new ApiResponse(HttpStatus.OK,
                    "Produit supprimée avec succès. "), HttpStatus.OK);
        }catch(Exception e) {
            return new ResponseEntity<Object>(new ApiResponse(HttpStatus.BAD_REQUEST,
                    "Erreur lors de la suppression du produit : " + e.getMessage()),HttpStatus.BAD_REQUEST);
        }

    }

    @PostMapping(path="/consume", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> createProduct(@RequestBody ConsumeDTO consumeDTO) throws JsonProcessingException {

        try {
            System.out.println("consumeDTO =" + consumeDTO.getId() + " - " + consumeDTO.getQuantity());

            if(productService.consume(consumeDTO)){
                return new ResponseEntity<Object>(new ApiResponse(HttpStatus.OK,
                        "Produit :" + consumeDTO.getId() +" mis à jour :"  + ", quantité:"+consumeDTO.getQuantity()),HttpStatus.OK);
            }else{
                return new ResponseEntity<Object>(new ApiResponse(HttpStatus.BAD_REQUEST,
                        "Produit non trouvé : "),HttpStatus.OK);
            }

        }
        catch(Exception e) {
            return new ResponseEntity<Object>(new ApiResponse(HttpStatus.BAD_REQUEST,
                    "Erreur lors de l'ajout du produit : " + e.getMessage()),HttpStatus.OK);
        }
    }

}
