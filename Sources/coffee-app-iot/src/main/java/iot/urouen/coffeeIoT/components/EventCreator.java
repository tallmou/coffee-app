package iot.urouen.coffeeIoT.components;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import iot.urouen.coffeeIoT.services.RestService;

@Component
public class EventCreator {
    @Autowired
    RestService restService;

    @Value("${iot.serverUrl}")
    String url;

    @Value("${iot.serverPort}")
    String port;

    @Value("${iot.machine}")
    String machineId;



    @Scheduled(fixedRateString = "${iot.frequency}")
    public void saveMachineState() {
        String portStr = (port.equals("80"))? "" : ":" + port;
        //run state save
        restService.synchronize(Long.parseLong(machineId), url
                + portStr+ "/api/products/synchronize");

    }

}
