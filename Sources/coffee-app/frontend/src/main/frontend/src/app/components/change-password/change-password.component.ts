import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UserService } from 'src/app/services/user.service';
import { UserStorage } from 'src/app/core/userstorage/user.storage';
import { ActivatedRoute, Router } from '@angular/router';

function passwordConfirming(c: AbstractControl): any {
  if (!c.parent || !c) {
    return;
  }
  const pwd = c.parent.get('password');
  const cpwd = c.parent.get('passwordConfirm');

  if (!pwd || !cpwd) {
    return;
  }
  if (pwd.value !== cpwd.value) {
    return { invalid: true };
  }
}

const ACTIVATION = 1;
const PWD_FORGOTTEN = 2;
const CHANGE_PWD = 3;

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {
  form: FormGroup;
  errorMessage: string;
  messageSuccessfull: string;
  token: string;

  type = CHANGE_PWD;
  constructor(
    private formBuilder: FormBuilder,
    private userStorage: UserStorage,
    private route: ActivatedRoute,
    private userService: UserService,
    private router: Router
  ) {}

  ngOnInit() {
    const url = this.router.url;

    this.type = url.startsWith('/activate') ? ACTIVATION : PWD_FORGOTTEN;
    const ke = 'email';
    const kt = 'token';
    this.route.params.subscribe(p => {
      if (p[ke] && p[kt]) {
        this.token = p[kt];
        this.form = this.formBuilder.group({
          email: [
            p[ke],
            [Validators.required, Validators.pattern('[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}')]
          ],
          password: [
            '',
            [Validators.required, Validators.pattern('(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}')]
          ],
          passwordConfirm: ['', [Validators.required, passwordConfirming]]
        });
      } else {
        this.router.navigate(['login']);
      }
    });
  }

  update({ value, valid }: { value: any; valid: boolean }) {
    if (valid) {
      if (this.type === ACTIVATION) {
        this.userService.activateAccount(value.email, value.password, this.token).subscribe(
          res => {
            const data: any = res;
            if (data.status === 'OK') {
              console.log('Successfull Change');
              this.messageSuccessfull =
                'Votre compte est maintenant activé. Utilisez votre adresse email pour vous connecter';
              this.form.reset();
              this.errorMessage = null;
            }
          },
          err => {
            console.log('Unsuccessfull Change');
            this.errorMessage = err;
            this.messageSuccessfull = null;
          }
        );
      }
      if (this.type === PWD_FORGOTTEN) {
        this.userService.changeForgottenPassword(value.email, value.password, this.token).subscribe(
          res => {
            const data: any = res;
            if (data.status === 'OK') {
              console.log('Successfull Change');
              this.messageSuccessfull = 'Vous avez changé votre mot de passe !';
              this.form.reset();
              this.errorMessage = null;
            }
          },
          err => {
            console.log('Unsuccessfull Change');
            this.errorMessage = err;
            this.messageSuccessfull = null;
          }
        );
      }
    } else {
      console.log('Unsuccessfull');
      this.errorMessage = 'Données invalides';
      this.messageSuccessfull = null;
    }
  }
}
