import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, AbstractControl } from '@angular/forms';
import { UserService } from '../../services/user.service';
import { User } from '../../models/user.model';

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.scss']
})
export class UserFormComponent implements OnInit {
  form: FormGroup;
  errorMessage: string;
  registrationSuccessful = false;
  constructor(private formBuilder: FormBuilder, private userService: UserService) {}

  ngOnInit() {
    this.form = this.formBuilder.group({
      email: ['', [Validators.required, Validators.pattern('[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}')]],
      // password: ['', [Validators.required, Validators.pattern("(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}")]],
      // passwordConfirm : ['', [Validators.required, passwordConfirming]],
      firstname: [''],
      lastname: ['']
    });
  }

  save({ value, valid }: { value: any; valid: boolean }) {
    if (valid) {
      const user = new User(value.firstname, value.lastname, value.email, null);

      this.userService.save(user).subscribe(
        res => {
          console.log(res);
          const data: any = res;
          if (data.status === 'OK') {
            console.log('Successfull Registration');
            this.registrationSuccessful = true;
            this.form.reset();
            this.errorMessage = null;
          }
        },
        err => {
          console.log('Unsuccessfull Registration');
          this.errorMessage = err;
          this.registrationSuccessful = false;
        }
      );
    } else {
      console.log('Unsuccessfull Registration');
      this.errorMessage = 'Données invalides';
      this.registrationSuccessful = false;
    }
  }
}
