import { Product } from './../../models/product.model';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { ProductService } from '../../services/product.service';
import { RoleService } from '../../core/role/role.service';
import { Router } from '@angular/router';
import { DialogService } from '../../services/dialog.service';
import { HttpEventType } from '@angular/common/http';
import * as FileSaver from 'file-saver';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent implements OnInit {
  // DataSource de Angular
  dataSource = new MatTableDataSource();
  // Les colonnes à mettre dans DataTable
  displayedColumnsUser: string[] = ['name', 'quantity', 'machine', 'edit', 'delete', 'alert'];
  displayedColumnsAdmin: string[] = ['name', 'quantity', 'machine', 'owner', 'alert'];

  searchKey: string;
  isUser: boolean;
  minQuantityAlert = 30;
  // MatSort et MatPaginator sont deux composant d'angular qui permettent de sort et paginer
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;

  // dans le constructeur on met les services à utiliser dans nos composent
  constructor(
    private productService: ProductService,
    private roleService: RoleService,
    private router: Router,
    private dialogService: DialogService
  ) {}

  // pour inialiser notre Datatable angular
  ngOnInit() {
    this.isUser = this.roleService.IsUser();
    this.loadData();
  }

  loadData() {
    this.productService.getAllProducts().subscribe(data => {
      console.log(data);
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }

  // on envoie toujours
  onEdit(id) {
    this.router.navigate(['/products/' + id]);
  }

  isOnAlert(ActualQuantity: number) {
    return ActualQuantity < this.minQuantityAlert;
  }

  applyFilter() {
    const search = this.searchKey.trim().toLowerCase();
    this.dataSource.filter = search;
  }

  onCreate() {
    this.router.navigate(['/products/add']);
  }

  onSearchClear() {
    this.searchKey = '';
  }

  onDelete(id) {
    this.dialogService
      .openConfirmDialog('Etes-vous de vouloir supprimer cet utilisateur ?')
      .afterClosed()
      .subscribe(val => {
        if (val) {
          this.productService.delete(id).subscribe(
            res => {
              const data: any = res;
              if (data.status === 'OK') {
                console.log('successfull');
                this.loadData();
              } else {
                console.log('unsuccessfull');
              }
            },
            err => {
              console.log('error');
            }
          );
        }
      });
  }

  getDocument() {
    this.productService.getProductsFile().subscribe(res => {
      if (res.type === HttpEventType.Response) {
        let documentData: any = res;
        console.log(documentData);
        if (documentData.statusText === 'OK') {
          documentData = documentData.body;
          if (documentData.type === 'application/json') {
            console.log("Vous n'avez pas les droits requis");
          } else {
            FileSaver.saveAs(documentData, 'products-exported');
          }
        } else {
          console.log('error');
        }
      }
    });
  }
}
