import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { AuthService } from '../../core/auth/auth.service';
import { Router } from '@angular/router';
import { TokenStorage } from '../../core/token/token.storage';
import { User } from '../../models/user.model';
import { UserStorage } from '../../core/userstorage/user.storage';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  form: FormGroup;
  errorMessage: string;
  user: User;
  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private router: Router,
    private token: TokenStorage,
    private userStorage: UserStorage
  ) {}

  ngOnInit() {
    if (this.authService.isAuthenticated()) {
      this.router.navigate(['home']);
    }
    this.form = this.formBuilder.group({
      email: ['', [Validators.required, Validators.pattern('[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}')]],
      password: ['', Validators.required]
    });
  }

  login({ value, valid }: { value: any; valid: boolean }) {
    this.authService.attemptAuth(value.email, value.password).subscribe(
      data => {
        if (data.status === 'OK') {
          const credentials = JSON.parse(data.message);

          this.token.saveToken(credentials.token);
          this.user = new User(
            credentials.user.firstName,
            credentials.user.lastName,
            credentials.user.email,
            credentials.user.role
          );

          this.user.id = credentials.user.id;
          this.authService.setCurrentUser(this.user);
          this.userStorage.saveUser(JSON.stringify(this.user));
          this.router.navigate(['home']);
        } else {
          this.errorMessage = data.message;
        }
      },
      error => {
        this.errorMessage = error.message;
      }
    );
  }

  forgottenPassword() {
    this.router.navigate(['password_forgotten']);
  }
}
