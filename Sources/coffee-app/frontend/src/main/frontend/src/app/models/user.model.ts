export enum UserType {
  PROVIDER,
  ADMINSIGMA,
  ADMINENTITE,
  PURCHASER,
  RESPONSABLE
}

export class User {
  id: number;
  role: any;
  email: string;
  password: string;
  firstName: string;
  lastName: string;

  constructor(firstName: string, lastName: string, mail: string, role: any) {
    this.role = role;
    this.email = mail;
    this.firstName = firstName;
    this.lastName = lastName;
  }
}
