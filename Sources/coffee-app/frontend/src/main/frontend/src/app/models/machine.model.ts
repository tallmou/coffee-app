import { User } from './user.model';

export class Machine {
  id: number;
  name: string;
  frequency: number;
  port: number;
  url: string;
  owner: User;
  version: string;
  userId: number;

  constructor(name: string, frequency: number, port: number, version: string) {
    this.name = name;
    this.frequency = frequency;
    this.port = port;
    this.version = version;
  }
}
