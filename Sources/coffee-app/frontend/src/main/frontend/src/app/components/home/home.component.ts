import { Component, OnInit, Input } from '@angular/core';
import { DomSanitizer, SafeResourceUrl, SafeHtml } from '@angular/platform-browser';
import { utils } from 'protractor';
import { UtilService } from '../../core/util/util.service';

@Component({
  moduleId: module.id,
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  @Input()
  id: string;
  urlMeteo: SafeResourceUrl = 'http://www.infoclimat.fr/api-previsions-meteo.html?id=2988507&cntry=FR';
  urlWeekend = 'https://estcequecestbientotleweekend.fr';
  urlPress = 'http://isitweekendyet.com/';
  dataWeekend: SafeHtml;
  dataPress: SafeHtml;

  constructor(public sanitizer: DomSanitizer, private utilService: UtilService) {}

  ngOnInit() {
    // this.utilService.getDynamicREST(this.urlWeekend).subscribe((res)=> {
    //   console.log( this.dataWeekend );
    //   this.dataWeekend = this.sanitizer.bypassSecurityTrustHtml(res);
    // })
    // this.utilService.getDynamicREST(this.urlPress).subscribe((res)=> {
    //   this.dataPress = this.sanitizer.bypassSecurityTrustHtml(res);
    // })
    // this.urlMeteo = this.sanitizer.bypassSecurityTrustResourceUrl(this.id);
  }
}
