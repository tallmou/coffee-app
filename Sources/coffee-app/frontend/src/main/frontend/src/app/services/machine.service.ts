import { Injectable } from '@angular/core';
import { UrlConfig } from '../core/config/url-config';
import { Machine } from '../models/machine.model';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class MachineService {
  constructor(private http: HttpClient) {}

  machineUrl: string = UrlConfig.API_URL + '/api/machines';
  getAllmachines() {
    return this.http.get<any[]>(this.machineUrl);
  }

  save(machine: any) {
    return this.http.post(this.machineUrl, machine);
  }

  getById(id: number) {
    return this.http.get<Machine>(this.machineUrl + '/' + id);
  }

  update(machine: Machine) {
    return this.http.put(this.machineUrl + '/' + machine.id, machine);
  }

  affect(idUser: number, idMachine: number) {
    return this.http.put(this.machineUrl + '/assign/' + idMachine, idUser);
  }

  delete(id: number) {
    return this.http.delete(this.machineUrl + '/' + id);
  }
}
