import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UserService } from 'src/app/services/user.service';
import { MachineService } from 'src/app/services/machine.service';
import { map } from 'rxjs/operators';
import { UrlConfig } from '../../core/config/url-config';

@Component({
  selector: 'app-machine-form',
  templateUrl: './machine-form.component.html',
  styleUrls: ['./machine-form.component.scss']
})
export class MachineFormComponent implements OnInit {
  saveSuccessfull: boolean;
  errorMessage: any;
  constructor(private fb: FormBuilder, private userService: UserService, private machineService: MachineService) {}

  users: any;
  form: FormGroup;

  ngOnInit() {
    this.users = this.userService.getAllUsers().pipe(
      map(data => {
        const items = data as any[];
        items.forEach(item => {
          item.userName = item.firstName + ' ' + item.lastName + '   (' + item.email + ')';
        });
        return items;
      })
    );

    this.form = this.fb.group({
      name: ['', [Validators.required]],
      port: [UrlConfig.IOT_PORT, [Validators.required, Validators.pattern('[0-9]+')]],
      url: [UrlConfig.IOT_URL, [Validators.required]],
      frequency: [null],
      userId: [null, [Validators.required]],
      version: ['']
    });
  }

  save({ value, valid }: { value: any; valid: boolean }) {
    if (valid) {
      this.machineService.save(value).subscribe(
        res => {
          console.log(res);
          const data: any = res;
          if (data.status === 'OK') {
            console.log('Successfull Save Machine');
            this.saveSuccessfull = true;
            this.form.reset();
            this.errorMessage = null;
          }
        },
        err => {
          console.log('Unsuccessfull Save Machine');
          this.errorMessage = err;
          this.saveSuccessfull = false;
        }
      );
    } else {
      console.log('Unsuccessfull Save machine');
      this.errorMessage = 'Données invalides';
      this.saveSuccessfull = false;
    }
  }
}
