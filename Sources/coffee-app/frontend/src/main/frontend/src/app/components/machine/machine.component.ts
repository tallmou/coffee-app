import { UserService } from './../../services/user.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { MachineService } from '../../services/machine.service';
import { Machine } from '../../models/machine.model';

@Component({
  selector: 'app-machine',
  templateUrl: './machine.component.html',
  styleUrls: ['./machine.component.scss']
})
export class MachineComponent implements OnInit {
  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private machineService: MachineService,
    private router: Router,
    private userService: UserService
  ) {}
  machine: any;
  machineid: number;
  form: FormGroup;

  errorMessage: string;
  editSuccessful = false;
  editError = false;

  ngOnInit() {
    const id = 'id';
    this.form = this.formBuilder.group({
      name: ['', [Validators.required]],
      port: [null, [Validators.required, Validators.pattern('[0-9]+')]],
      url: [null, [Validators.required]],
      frequency: [null],
      version: ['']
    });
    this.route.params.subscribe(p => {
      this.machineid = p[id];
      this.machineService.getById(this.machineid).subscribe(res => {
        this.machine = res;
        this.loadData();
      });
    });
  }
  loadData() {
    this.form.setValue({
      name: this.machine.name,
      frequency: this.machine.frequency,
      port: this.machine.port,
      url: this.machine.url,
      version: this.machine.version
    });
  }

  edit({ value, valid }: { value: any; valid: boolean }) {
    const machine = new Machine(value.name, value.frequency, value.port, value.version);
    machine.id = this.machine.id;
    machine.userId = this.machine.owner.id;
    machine.url = value.url;
    console.log(machine);
    this.machineService.update(machine).subscribe(
      res => {
        const data: any = res;
        if (data.status === 'OK') {
          console.log('Successfull machine updated');
          this.editSuccessful = true;
          this.errorMessage = null;
        }
      },
      err => {
        this.errorMessage = err;
        this.editError = true;
      }
    );
  }
}
