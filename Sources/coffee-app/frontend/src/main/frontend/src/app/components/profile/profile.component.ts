import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormGroup, Validators, FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';
import { User } from 'src/app/models/user.model';
import { UserStorage } from 'src/app/core/userstorage/user.storage';

function passwordConfirming(c: AbstractControl): any {
  if (!c.parent || !c) {
    return;
  }
  const pwd = c.parent.get('password');
  const cpwd = c.parent.get('passwordConfirm');

  if (!pwd || !cpwd) {
    return;
  }
  if (pwd.value !== cpwd.value) {
    return { invalid: true };
  }
}

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  constructor(
    private formBuilder: FormBuilder,
    private userStorage: UserStorage,
    private route: ActivatedRoute,
    private userService: UserService,
    private router: Router
  ) {}

  formPwd: FormGroup;
  userid: number;
  form: FormGroup;
  editSuccessful = false;
  editError = false;
  errorMessage: string;
  type = 'user';
  email: string;
  errorPwdMessage: string;
  editPwdSuccessfull = false;
  ngOnInit() {
    const id = 'id';
    this.route.params.subscribe(p => {
      this.userid = p[id];
      let user: User;
      if (this.userid) {
        this.type = 'admin';
        this.userService.getById(this.userid).subscribe(res => {
          user = res;
          if (!user) {
            this.router.navigate(['login']);
          }
          this.form = this.formBuilder.group({
            email: [user.email],
            firstname: [user.firstName],
            lastname: [user.lastName]
          });
        });
      } else {
        this.formPwd = this.formBuilder.group({
          password: [
            '',
            [Validators.required, Validators.pattern('(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}')]
          ],
          passwordConfirm: ['', [Validators.required, passwordConfirming]]
        });
        const userJson = JSON.parse(this.userStorage.getUser());
        user = new User(userJson.firstName, userJson.lastName, userJson.email, userJson.role);
        if (!user) {
          this.router.navigate(['login']);
        }
        this.userid = userJson.id;
        this.email = user.email;
        this.form = this.formBuilder.group({
          email: [{ value: user.email, disabled: true }],
          firstname: [user.firstName],
          lastname: [user.lastName]
        });
      }
    });
  }

  edit({ value, valid }: { value: any; valid: boolean }) {
    if (valid) {
      const user = new User(value.firstname, value.lastname, value.email, null);
      user.id = this.userid;
      this.userService.update(user).subscribe(
        res => {
          const data: any = res;
          if (data.status === 'OK') {
            console.log('Successfull Registration');
            this.editSuccessful = true;
            this.errorMessage = null;
          }
        },
        err => {
          console.log('Unsuccessfull Registration');
          this.errorMessage = err;
          this.editError = true;
        }
      );
    } else {
      console.log('Unsuccessfull Registration');
      this.errorMessage = 'Données invalides';
      this.editError = true;
    }
  }

  updatePwd({ value, valid }: { value: any; valid: boolean }) {
    if (valid) {
      this.userService.changePassword(this.email, value.password).subscribe(
        res => {
          console.log(res);
          const data: any = res;
          if (data.status === 'OK') {
            console.log('Successfull Change');
            this.editPwdSuccessfull = true;
            this.errorPwdMessage = null;
            this.form.reset();
          }
        },
        err => {
          console.log('Unsuccessfull Change');
          this.errorPwdMessage = err;
        }
      );
    } else {
      console.log('Unsuccessfull Registration');
      this.errorPwdMessage = 'Données invalides';
      this.editError = true;
    }
  }
}
