import { MachineService } from './../../services/machine.service';
import { Machine } from './../../models/machine.model';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UserService } from 'src/app/services/user.service';
import { MatDialogRef } from '@angular/material';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-machine-affect',
  templateUrl: './machine-affect.component.html',
  styleUrls: ['./machine-affect.component.scss']
})
export class MachineAffectComponent implements OnInit {
  constructor(
    private fb: FormBuilder,
    private userService: UserService,
    public dialogRef: MatDialogRef<MachineAffectComponent>,
    private machineService: MachineService
  ) {}

  users: any;
  machine: Machine;
  form: FormGroup;
  assignSuccessfull = false;
  errorMessage: string;

  ngOnInit() {
    this.users = this.userService.getAllUsers().pipe(
      map(data => {
        const items = data as any[];
        items.forEach(item => {
          item.userName = item.firstName + ' ' + item.lastName + '   (' + item.email + ')';
        });
        console.log(items[0].userName);
        return items;
      })
    );
    let id = null;
    if (this.machine.owner) {
      id = this.machine.owner.id;
    }
    this.form = this.fb.group({
      user: [id, [Validators.required]]
    });
  }

  onSubmit({ value, valid }: { value: any; valid: boolean }) {
    this.machineService.affect(value.user, this.machine.id).subscribe(
      res => {
        const data: any = res;
        if (data.status === 'OK') {
          console.log('Successfull Registration');
          this.assignSuccessfull = true;
          this.errorMessage = null;
        }
      },
      err => {
        console.log('Unsuccessfull Registration');
        this.errorMessage = err;
        this.assignSuccessfull = false;
      }
    );
  }

  onClose() {
    this.dialogRef.close();
  }
}
