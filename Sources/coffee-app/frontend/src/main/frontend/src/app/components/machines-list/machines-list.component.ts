import { Router } from '@angular/router';
import { MachineAffectComponent } from './../machine-affect/machine-affect.component';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatSort, MatPaginator, MatDialogConfig, MatDialog } from '@angular/material';
import { MachineService } from '../../services/machine.service';
import { RoleService } from '../../core/role/role.service';
import { DialogService } from '../../services/dialog.service';

@Component({
  selector: 'app-machines-list',
  templateUrl: './machines-list.component.html',
  styleUrls: ['./machines-list.component.scss']
})
export class MachinesListComponent implements OnInit {
  dataSource = new MatTableDataSource();
  // Les colonnes à afficher dans la datatable
  displayedColumns: string[] = ['id', 'name', 'port', 'owner', 'affect', 'edit', 'delete'];
  searchKey: string;
  isUser: boolean;
  constructor(
    private roleService: RoleService,
    private router: Router,
    private machineService: MachineService,
    private dialog: MatDialog,
    private dialogService: DialogService
  ) {}

  @ViewChild(MatSort, { static: false }) sort: MatSort;
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  ngOnInit() {
    this.isUser = this.roleService.IsUser();
    this.loadData();
  }
  loadData() {
    this.machineService.getAllmachines().subscribe(data => {
      data.forEach(item => {
        console.log(item);
        if (item.owner) {
          item.ownerName = item.owner.firstName + ' ' + item.owner.lastName;
        } else {
          item.ownerName = '';
        }
      });
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }
  applyFilter() {
    const search = this.searchKey.trim().toLowerCase();
    this.dataSource.filter = search;
  }

  onCreate() {
    this.router.navigate(['/machines/add']);
  }

  getDocument() {}
  onSearchClear() {
    this.searchKey = '';
  }

  onEdit(machine) {
    console.log(machine);
    this.router.navigate(['/machines/edit', machine.id]);
  }

  onAffect(row) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    dialogConfig.width = '50%';
    const dialogRef = this.dialog.open(MachineAffectComponent, dialogConfig);
    dialogRef.componentInstance.machine = row;
    dialogRef.afterClosed().subscribe(result => {
      this.loadData();
      console.log('The dialog was closed');
    });
  }

  onDelete(id) {
    this.dialogService
      .openConfirmDialog('Etes-vous de vouloir supprimer cet utilisateur ?')
      .afterClosed()
      .subscribe(val => {
        if (val) {
          this.machineService.delete(id).subscribe(
            res => {
              const data: any = res;
              if (data.status === 'OK') {
                console.log('successfull');
                this.loadData();
              } else {
                console.log('unsuccessfull');
              }
            },
            err => {
              console.log('error');
            }
          );
        }
      });
  }
}
