import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { UserService } from '../../services/user.service';
import { DialogService } from '../../services/dialog.service';
import { Router } from '@angular/router';
import { HttpEventType } from '@angular/common/http';
import * as FileSaver from 'file-saver';
@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {
  dataSource = new MatTableDataSource();
  displayedColumns: string[] = ['firstName', 'lastName', 'email', 'edit', 'delete'];
  searchKey: string;

  constructor(private userService: UserService, private router: Router, private dialogService: DialogService) {}

  @ViewChild(MatSort, { static: false }) sort: MatSort;
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;

  ngOnInit() {
    this.loadData();
  }
  loadData() {
    this.userService.getAllUsers().subscribe(data => {
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }

  applyFilter() {
    const search = this.searchKey.trim().toLowerCase();
    this.dataSource.filter = search;
  }

  onCreate() {
    this.router.navigate(['/register']);
  }

  onSearchClear() {
    this.searchKey = '';
  }

  onEdit(id) {
    console.log(id);
    this.router.navigate(['/profile', id]);
  }

  onDelete(id) {
    this.dialogService
      .openConfirmDialog('Etes-vous de vouloir supprimer cet utilisateur ?')
      .afterClosed()
      .subscribe(val => {
        if (val) {
          this.userService.delete(id).subscribe(
            res => {
              const data: any = res;
              if (data.status === 'OK') {
                console.log('successfull');
                this.loadData();
              } else {
                console.log('unsuccessfull');
              }
            },
            err => {
              console.log('error');
            }
          );
        }
      });
  }

  getDocument() {
    this.userService.getUsersFile().subscribe(res => {
      if (res.type === HttpEventType.Response) {
        let documentData: any = res;
        console.log(documentData);
        if (documentData.statusText === 'OK') {
          documentData = documentData.body;
          if (documentData.type === 'application/json') {
            console.log("Vous n'avez pas les droits requis");
          } else {
            FileSaver.saveAs(documentData, 'users-exported');
          }
        } else {
          console.log('error');
        }
      }
    });
  }
}
