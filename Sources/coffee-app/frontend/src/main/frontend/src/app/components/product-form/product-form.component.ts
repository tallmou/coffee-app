import { Product } from './../../models/product.model';
import { MachineService } from 'src/app/services/machine.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ProductService } from './../../services/product.service';
import { Component, OnInit } from '@angular/core';
import { map } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-product-form',
  templateUrl: './product-form.component.html',
  styleUrls: ['./product-form.component.scss']
})
export class ProductFormComponent implements OnInit {
  messageSuccessfull: string;
  errorMessage: any;
  form: FormGroup;
  machines: any;
  product: Product;
  title: string;
  constructor(
    private productService: ProductService,
    private fb: FormBuilder,
    private machineService: MachineService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit() {
    this.machines = this.machineService.getAllmachines().pipe(
      map(data => {
        const items = data as any[];
        return items;
      })
    );

    this.form = this.fb.group({
      name: ['', [Validators.required]],
      quantity: [null, [Validators.required, Validators.pattern('[0-9]+')]],
      price: [null, [Validators.required, Validators.pattern('^[0-9]+(\\.[0-9]+)?$')]],
      machineId: [null, [Validators.required]]
    });
    this.title = 'Ajout de produit';
    const id = 'id';
    this.route.params.subscribe(p => {
      const productId = p[id];

      if (!isNaN(productId)) {
        this.title = 'Modification de produit';
        this.productService.getById(productId).subscribe(res => {
          this.product = res;
          if (!this.product) {
            this.router.navigate(['login']);
          }
          this.form.setValue({
            name: this.product.name,
            quantity: this.product.quantity,
            price: this.product.price,
            machineId: this.product.machine.id
          });
        });
      }
    });
  }

  submit({ value, valid }: { value: any; valid: boolean }) {
    if (valid) {
      if (!this.product) {
        // Ajout
        this.productService.save(value).subscribe(
          res => {
            console.log(res);
            const data: any = res;
            if (data.status === 'OK') {
              console.log('Successfull Save Machine');
              this.messageSuccessfull = 'Ajout de produit réussi';
              this.form.reset();
              this.errorMessage = null;
            }
          },
          err => {
            console.log('Unsuccessfull Save Machine');
            this.errorMessage = err;
            this.messageSuccessfull = null;
          }
        );
      } else {
        // Modification
        value.id = this.product.id;
        value.machineId = this.product.machine.id;
        this.productService.update(value).subscribe(
          res => {
            console.log(res);
            const data: any = res;
            if (data.status === 'OK') {
              console.log('Successfull Update Machine');
              this.messageSuccessfull = 'Modification de produit réussie';
              this.errorMessage = null;
            }
          },
          err => {
            console.log('Unsuccessfull Save Machine');
            this.errorMessage = err;
            this.messageSuccessfull = null;
          }
        );
      }
    } else {
      console.log('Unsuccessfull Save machine');
      this.errorMessage = 'Données invalides';
      this.messageSuccessfull = null;
    }
  }
}
