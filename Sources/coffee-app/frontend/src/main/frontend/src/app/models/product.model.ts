import { Machine } from './machine.model';

export class Product {
  id: number;
  name: string;
  quantity: number;
  price: number;
  machine: Machine;
  machineId: number;
  constructor(name: string, quantity: number) {
    this.name = name;
    this.quantity = quantity;
  }
}
