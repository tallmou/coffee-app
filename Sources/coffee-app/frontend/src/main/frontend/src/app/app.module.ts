import { BrowserModule } from '@angular/platform-browser';
import { NgModule, NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { MatFormFieldModule } from '@angular/material/form-field';
import {
  MatButtonModule,
  MatSortModule,
  MatIconModule,
  MatCheckboxModule,
  MatToolbarModule,
  MatInputModule,
  MatCardModule,
  MatMenuModule,
  MatTableModule,
  MatPaginatorModule,
  MatListModule
} from '@angular/material';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { MatDialogModule, MatDialogContent } from '@angular/material/dialog';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgSelectModule } from '@ng-select/ng-select';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './components/login/login.component';
import { AuthService } from './core/auth/auth.service';
import { Interceptor } from './core/interceptor/interceptor';
import { RoleService } from './core/role/role.service';
import { TokenStorage } from './core/token/token.storage';
import { UserStorage } from './core/userstorage/user.storage';
import { AuthGuard } from './core/auth/auth.guard';
import { UserFormComponent } from './components/user-form/user-form.component';
import { UserService } from './services/user.service';
import { UserListComponent } from './components/user-list/user-list.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { HomeComponent } from './components/home/home.component';
import { ProfileComponent } from './components/profile/profile.component';
import { MachinesListComponent } from './components/machines-list/machines-list.component';
import { ProductListComponent } from './components/product-list/product-list.component';
import { RoleGuard } from './core/role/role.guard';
import { ChangePasswordComponent } from './components/change-password/change-password.component';
import { PasswordForgottenComponent } from './components/password-forgotten/password-forgotten.component';
import { MachineAffectComponent } from './components/machine-affect/machine-affect.component';
import { ProductFormComponent } from './components/product-form/product-form.component';
import { MachineFormComponent } from './components/machine-form/machine-form.component';
import { MachineComponent } from './components/machine/machine.component';
import { MatConfirmDialogComponent } from './components/mat-confirm-dialog/mat-confirm-dialog.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    UserFormComponent,
    UserListComponent,
    NavbarComponent,
    HomeComponent,
    ProfileComponent,
    MachinesListComponent,
    ProductListComponent,
    ProductListComponent,
    ChangePasswordComponent,
    PasswordForgottenComponent,
    MachineAffectComponent,
    ProductFormComponent,
    MachineFormComponent,
    MachineComponent,
    MatConfirmDialogComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    MatButtonModule,
    HttpClientModule,
    CommonModule,
    MatIconModule,
    MatCheckboxModule,
    MatToolbarModule,
    MatInputModule,
    MatTableModule,
    MatPaginatorModule,
    MatCardModule,
    MatMenuModule,
    MatSortModule,
    MatDialogModule,
    MatListModule,
    BrowserAnimationsModule,
    NgSelectModule
  ],
  providers: [
    AuthService,
    RoleService,
    TokenStorage,
    UserStorage,
    AuthGuard,
    RoleGuard,
    UserService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: Interceptor,
      multi: true
    }
  ],
  entryComponents: [MachineAffectComponent, MatConfirmDialogComponent],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA]
})
export class AppModule {}
