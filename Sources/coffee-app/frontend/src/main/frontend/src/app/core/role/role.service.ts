import { Injectable } from '@angular/core';
import { TokenStorage } from '../token/token.storage';
import decode from 'jwt-decode';

@Injectable()
export class RoleService {
  constructor(public tokenStorage: TokenStorage) {}

  getRole(): string {
    const token = this.tokenStorage.getToken();
    if (token != null) {
      return decode(token).role;
    } else {
      return null;
    }
  }

  canActivate(requiredRole: string): boolean {
    return this.getRole() === requiredRole;
  }

  IsAdmin() {
    return this.canActivate('ROLE_ADMIN');
  }
  IsUser() {
    return this.canActivate('ROLE_USER');
  }
}
