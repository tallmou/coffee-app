import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpClientModule } from '@angular/common/http';
import { SafeResourceUrl } from '@angular/platform-browser';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UtilService {
  optionRequete = {
    headers: new HttpHeaders({
      'Access-Control-Allow-Origin': '*',
      'mon-entete-personnalise': 'maValeur'
    })
  };

  constructor(private http: HttpClient) {}
  public getDynamicREST(url: string) {
    return this.http.get<any>(url, this.optionRequete);
  }
}
