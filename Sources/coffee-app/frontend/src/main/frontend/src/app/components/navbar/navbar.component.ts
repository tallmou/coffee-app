import { UserStorage } from 'src/app/core/userstorage/user.storage';
import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/core/auth/auth.service';
import { User } from '../../models/user.model';
import { RoleService } from '../../core/role/role.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  error: string;
  userName: string;
  constructor(
    private roleService: RoleService,
    private authService: AuthService,
    private router: Router,
    private userStorage: UserStorage
  ) {
    if (this.authService.isAuthenticated()) {
      const user = JSON.parse(this.userStorage.getUser());
      this.userName = user.firstName + ' ' + user.lastName;
      console.log(this.userName);
    }
  }

  ngOnInit() {}

  logout() {
    this.authService.logout();
  }

  isAdmin() {
    return this.roleService.IsAdmin();
  }

  isUser() {
    return this.roleService.IsUser();
  }
}
