import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest } from '@angular/common/http';
import { User } from '../models/user.model';
import { UrlConfig } from '../core/config/url-config';

@Injectable()
export class UserService {
  constructor(private http: HttpClient) {}

  userUrl = UrlConfig.API_URL + '/api/users';

  getAllUsers() {
    return this.http.get<User[]>(this.userUrl);
  }

  save(user: User) {
    return this.http.post(this.userUrl, user);
  }

  getById(id: number) {
    return this.http.get<User>(this.userUrl + '/' + id);
  }

  update(user: User) {
    return this.http.put(this.userUrl + '/' + user.id, user);
  }

  delete(id: number) {
    return this.http.delete(this.userUrl + '/' + id);
  }

  passwordForgotten(email: string) {
    return this.http.get<any>(this.userUrl + '/password_forgotten?email=' + email);
  }

  activateAccount(email: any, password: any, token: string) {
    const credentials = { email, password };
    return this.http.put(this.userUrl + '/activate/' + token, credentials);
  }

  changePassword(email: any, password: any) {
    const credentials = { email, password };
    return this.http.put(this.userUrl + '/change_password', credentials);
  }

  changeForgottenPassword(email: any, password: any, token: string) {
    const credentials = { email, password };
    return this.http.put(this.userUrl + '/change_password/forgotten/' + token, credentials);
  }

  getUsersFile() {
    const req = new HttpRequest('GET', this.userUrl + '/export', {
      responseType: 'blob'
    });

    return this.http.request(req);
  }
}
