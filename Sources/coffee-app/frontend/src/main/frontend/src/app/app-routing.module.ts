import { PasswordForgottenComponent } from './components/password-forgotten/password-forgotten.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { UserFormComponent } from './components/user-form/user-form.component';
import { UserListComponent } from './components/user-list/user-list.component';
import { HomeComponent } from './components/home/home.component';
import { ProfileComponent } from './components/profile/profile.component';
import { MachinesListComponent } from './components/machines-list/machines-list.component';
import { ProductListComponent } from './components/product-list/product-list.component';
import { RoleGuard } from './core/role/role.guard';
import { AuthGuard } from './core/auth/auth.guard';
import { ChangePasswordComponent } from './components/change-password/change-password.component';
import { ProductFormComponent } from './components/product-form/product-form.component';
import { MachineFormComponent } from './components/machine-form/machine-form.component';
import { MachineComponent } from './components/machine/machine.component';

const routes: Routes = [
  { path: 'machines', component: MachinesListComponent, canActivate: [AuthGuard] },
  {
    path: 'machines/edit/:id',
    component: MachineComponent,
    canActivate: [AuthGuard]
  },
  { path: 'products', component: ProductListComponent, canActivate: [AuthGuard] },
  {
    path: 'products/add',
    component: ProductFormComponent,
    canActivate: [RoleGuard, AuthGuard],
    data: { role: 'ROLE_USER' }
  },
  {
    path: 'products/:id',
    component: ProductFormComponent,
    canActivate: [RoleGuard, AuthGuard],
    data: { role: 'ROLE_USER' }
  },
  {
    path: 'machines/add',
    component: MachineFormComponent,
    canActivate: [RoleGuard, AuthGuard],
    data: { role: 'ROLE_ADMIN' }
  },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: UserFormComponent, canActivate: [RoleGuard, AuthGuard], data: { role: 'ROLE_ADMIN' } },
  { path: 'users', component: UserListComponent, canActivate: [RoleGuard, AuthGuard], data: { role: 'ROLE_ADMIN' } },
  { path: 'home', component: HomeComponent },
  {
    path: 'profile/:id',
    component: ProfileComponent,
    canActivate: [RoleGuard, AuthGuard],
    data: { role: 'ROLE_ADMIN' }
  },
  { path: 'profile', component: ProfileComponent, canActivate: [RoleGuard, AuthGuard], data: { role: 'ROLE_USER' } },
  { path: 'activate/:email/:token', component: ChangePasswordComponent },
  { path: 'change_password/forgotten/:email/:token', component: ChangePasswordComponent },
  { path: 'password_forgotten', component: PasswordForgottenComponent },

  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: '**', component: HomeComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
