export class UrlConfig {
  /*
    DEV URL
  */
  public static API_URL = 'http://localhost:8080';
  public static IOT_URL = 'http://localhost';
  public static IOT_PORT = 8090;
  /*
    PROD URL
  */
  // public static API_URL = "https://coffee-app-ufr.herokuapp.com";
  // public static IOT_URL = 'https://coffee-app-iot-ufr.herokuapp.com';
  // public static IOT_PORT = 80;
}
