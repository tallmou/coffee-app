import { Product } from './../models/product.model';
import { Injectable } from '@angular/core';
import { UrlConfig } from '../core/config/url-config';
import { HttpClient, HttpRequest } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  constructor(private http: HttpClient) {}

  productUrl: string = UrlConfig.API_URL + '/api/products';

  getAllProducts() {
    return this.http.get<Product[]>(this.productUrl);
  }

  save(product: Product) {
    return this.http.post(this.productUrl, product);
  }

  getById(id: number) {
    return this.http.get<Product>(this.productUrl + '/' + id);
  }

  update(product: Product) {
    return this.http.put(this.productUrl + '/' + product.id, product);
  }

  delete(id: number) {
    let url = this.productUrl + '/' + id;
    console.log('url=' + url);
    return this.http.delete(url.trim());
  }

  getProductsFile() {
    const req = new HttpRequest('GET', this.productUrl + '/export', {
      responseType: 'blob'
    });

    return this.http.request(req);
  }
}
