import { UserStorage } from './../userstorage/user.storage';
import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuthService } from '../auth/auth.service';
import { TokenStorage } from '../token/token.storage';
import decode from 'jwt-decode';
import { Observable } from 'rxjs';

@Injectable()
export class RoleGuard implements CanActivate {
  constructor(
    public auth: AuthService,
    public router: Router,
    public tokenStorage: TokenStorage,
    private userStorage: UserStorage
  ) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> | Promise<boolean> | boolean {
    const user = JSON.parse(this.userStorage.getUser());
    if (this.auth.isAuthenticated() && user.role.name === next.data.role) {
      return true;
    }

    this.router.navigate(['/home']);
    return false;
  }
}
