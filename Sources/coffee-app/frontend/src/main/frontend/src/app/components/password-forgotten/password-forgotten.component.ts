import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-password-forgotten',
  templateUrl: './password-forgotten.component.html',
  styleUrls: ['./password-forgotten.component.scss']
})
export class PasswordForgottenComponent implements OnInit {
  constructor(private formBuilder: FormBuilder, private userService: UserService) {}

  form: FormGroup;
  messageSuccessfull: string;
  errorMessage: string;
  ngOnInit() {
    this.form = this.formBuilder.group({
      email: ['', [Validators.required, Validators.pattern('[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}')]]
    });
  }

  submit({ value, valid }: { value: any; valid: boolean }) {
    if (valid) {
      this.userService.passwordForgotten(value.email).subscribe(
        res => {
          console.log(res);
          const data: any = res;
          if (data.status === 'OK') {
            console.log('Successfull');
            this.messageSuccessfull =
              "Vous allez recevoir d'une minute à l'autre un email pour choisir un nouveau mot de passe";
            this.form.reset();
            this.errorMessage = null;
          } else {
            this.errorMessage = data.message;
            this.messageSuccessfull = null;
          }
        },
        err => {
          console.log('Unsuccessfull ');
          this.errorMessage = err;
          this.messageSuccessfull = null;
        }
      );
    } else {
      this.errorMessage = 'Données invalides';
      this.messageSuccessfull = null;
    }
  }
}
