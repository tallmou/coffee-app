package backend.models;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.Setter;

@JsonIgnoreProperties(value = { "machines" })
@Entity
@Table(uniqueConstraints={@UniqueConstraint(columnNames={"email"})})
public class User {
	@Id
	@Getter @Setter
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@Getter @Setter
	private String firstName;
	
	@Getter @Setter
	private String lastName;
	
	@Getter @Setter
	@NotBlank(message = "Email obligatoire")
	private String email;
	
	@Getter @Setter
	private String password;
	
	@Getter @Setter
	private boolean active = false;
	
	@Getter @Setter
	@ManyToOne
	@JoinColumn(name = "role_id")
	private Role role;
	
	@Getter @Setter
	@OneToMany (fetch = FetchType.LAZY)
	@JoinColumn(name = "owner_id")
	private List<Machine> machines;	
}
