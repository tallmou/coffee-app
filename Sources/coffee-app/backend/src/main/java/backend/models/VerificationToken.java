package backend.models;

import javax.persistence.*;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.Calendar;
import java.util.UUID;
import java.sql.Timestamp;


@Entity
public class VerificationToken {
  private static final int EXPIRATION = 60 * 24;

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;
  
  @Getter @Setter
  private String token;

  @OneToOne(targetEntity = User.class, fetch = FetchType.EAGER)
  @JoinColumn(nullable = false, name = "user_id")
  @Getter @Setter
  private User user;

  @Getter @Setter
  private Date expiryDate;

  private Date calculateExpiryDate(int expiryTimeInMinutes) {
    Calendar cal = Calendar.getInstance();
    cal.setTime(new Timestamp(cal.getTime().getTime()));
    cal.add(Calendar.MINUTE, expiryTimeInMinutes);
    return new Date(cal.getTime().getTime());
  }

  public VerificationToken() { }

  public VerificationToken(User user) {
    this.user = user;
    this.token = UUID.randomUUID().toString();
    this.expiryDate = this.calculateExpiryDate(EXPIRATION);
  }

  
}
