package backend.models;

import backend.models.User;

public class AuthToken {

    private String token;
    private User user;

    public AuthToken() {

    }

    public AuthToken(String token, User user) {
        this.token = token;
        this.user = user;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public User getUser() {
        return this.user;
    }

    public void setUser(User u) {
        this.user = u;
    }

}
