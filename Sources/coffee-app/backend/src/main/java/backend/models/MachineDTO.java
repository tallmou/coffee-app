package backend.models;

import lombok.Getter;
import lombok.Setter;

public class MachineDTO {
	@Getter @Setter
	private int id;
	
	@Getter @Setter
	private int port;
	
	@Getter @Setter
	private String name;
	
	@Getter @Setter
	private int frequency;
	
	@Getter @Setter
	private String version;
	
	@Getter @Setter
	private String url;
	
	@Getter @Setter
	private long userId;

}
