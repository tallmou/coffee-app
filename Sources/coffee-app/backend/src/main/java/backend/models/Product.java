package backend.models;



import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.springframework.hateoas.Identifiable;

import lombok.Getter;
import lombok.Setter;


@Entity
public class Product implements Identifiable<Long> {
	@Id
	@Getter @Setter
	@GenericGenerator(name="IdOrGenerated",
	strategy="backend.utils.ProductIdOrGenerate"
			)
	@GeneratedValue(strategy=GenerationType.IDENTITY, generator="IdOrGenerated")
	private Long id;

	public Product() {}
	public Product(String name, double quantity,double price, Machine machine) {
		super();
		this.name = name;
		this.quantity = quantity;
		this.machine = machine;
		this.price=price;
	}

	@Getter @Setter
	private String name;

	@Getter @Setter
	private double quantity;

	@Getter @Setter
	private double price;

	@Getter @Setter
	@ManyToOne
	private Machine machine;

}
