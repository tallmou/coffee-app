package backend.models;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.Setter;


@JsonIgnoreProperties(value = { "products" })
@Entity
@Table(uniqueConstraints={@UniqueConstraint(columnNames={"port"})})
public class Machine {
	
	@Id
	@Getter @Setter
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@Getter @Setter
	private int port;
	
	@Getter @Setter
	private String name;
	
	@Getter @Setter
	private int frequency;
	
	@Getter @Setter
	private String version;
	
	@Getter @Setter
	private String url;
	
	@Getter @Setter
	@ManyToOne
	private User owner;
	
	@OneToMany
	@Getter @Setter
	@JoinColumn(name = "machine_id")
	private List<Product> products;
	
	public Machine() {}
	public Machine(String url, int port, String name, int frequency, String version, User owner) {
		super();
		this.url = url;
		this.port = port;
		this.name = name;
		this.frequency = frequency;
		this.version = version;
		this.owner = owner;
	}
	
	public Machine(int id, int port, String name, int frequency, String version) {
		super();
		this.port = port;
		this.name = name;
		this.frequency = frequency;
		this.version = version;
		this.id = id;
	}

}
