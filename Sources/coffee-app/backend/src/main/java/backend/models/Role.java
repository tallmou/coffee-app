package backend.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Getter;
import lombok.Setter;

@Entity
public class Role {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Getter @Setter
  private Long id;
  
  @Getter @Setter
  private String name;


  public Role() {}

  public Role(String name) {
    this.name = name;
  }

}
