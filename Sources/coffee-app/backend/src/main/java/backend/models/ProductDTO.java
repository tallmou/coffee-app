package backend.models;


import lombok.Getter;
import lombok.Setter;

public class ProductDTO {
	
	@Getter @Setter
	private long id;
	
	@Getter @Setter
	private String name;
	
	@Getter @Setter
	private double quantity;
	
	@Getter @Setter
	private double price;
	
	@Getter @Setter
	private long machineId;

}
