package backend.models;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

public class SynchronizationDTO {
	@Getter @Setter
	private long machineId;
	
	@Getter @Setter
	private List<Product> products;

	public SynchronizationDTO(long machineId, List<Product> products) {
		this.machineId = machineId;
		this.products = products;
	}
	
	public SynchronizationDTO() {
		
	}

	
}
