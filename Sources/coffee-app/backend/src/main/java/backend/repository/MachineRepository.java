package backend.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.PagingAndSortingRepository;

import backend.models.Machine;
import backend.models.User;


public interface MachineRepository extends PagingAndSortingRepository<Machine, Long> {	
	
	 Optional<Machine> findById(long id);

	 Machine findByPort(int port);
	 
	 List<Machine> findByOwner(User user);

}
