package backend.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Service;

import backend.models.User;
import backend.models.VerificationToken;



@Service

public interface VerificationTokenRepository extends PagingAndSortingRepository<VerificationToken, Long> {
  VerificationToken findByUser(User user);
  VerificationToken findByToken(String token);
}
