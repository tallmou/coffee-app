package backend.repository;

import java.util.Optional;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;

import backend.models.User;

@Service
public interface UserRepository extends PagingAndSortingRepository<User, Long> {
 
	 User findByEmail(@Param("email") String mail);

	 Optional<User> findById(long id);
}
