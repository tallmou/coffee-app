package backend.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import backend.models.Role;

public interface RoleRepository extends PagingAndSortingRepository<Role, Long>{
	Role findByName(String name);

}
