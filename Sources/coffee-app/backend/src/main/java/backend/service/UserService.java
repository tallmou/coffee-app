package backend.service;



import java.util.Collection;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import backend.models.LoginUser;
import backend.models.User;

public interface UserService {

    User save(User user) throws Exception;
    
    User update(User user) throws Exception;

    List<User> findAll();

    void delete(long id);

    User findOne(String email);

    User findById(Long id);
    
    User findByEmail(String email);
    
    UserDetails loadUserByUsername(String username) throws UsernameNotFoundException;
    
    Collection<? extends GrantedAuthority> getAuthorities();
    
    void activateAccount(LoginUser loginUser) throws Exception;
    
    void changePassword(LoginUser loginUser) throws Exception;
}
