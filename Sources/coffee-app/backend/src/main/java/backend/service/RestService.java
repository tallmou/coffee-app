package backend.service;


import java.util.Collections;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import backend.models.ProductDTO;

@Service
public class RestService {

    @Autowired
    ProductService productService;

    private final RestTemplate restTemplate;

    public RestService(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.build();
    }

    public ResponseEntity<String> addProduct(String url, ProductDTO p) {
        //get data
        HttpHeaders headers = new HttpHeaders();
        // set `accept` header
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));

        headers.set("x-request-source", "platform");

        HttpEntity<ProductDTO> entityToSend = new HttpEntity<>(p ,headers);

        //send data
        return this.restTemplate.postForEntity(url,entityToSend,String.class);
    }
    
    public void updateProduct(String url, ProductDTO p) {
  
        this.restTemplate.put(url ,p);
    }

	public void deleteProduct(String url) {
		// TODO Auto-generated method stub
		this.restTemplate.delete(url);
	}
}
