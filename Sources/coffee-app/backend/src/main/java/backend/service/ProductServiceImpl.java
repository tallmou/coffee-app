package backend.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import backend.models.Product;
import backend.repository.ProductRepository;

@Service(value = "productService")
public class ProductServiceImpl implements ProductService {
	
	@Autowired
	private ProductRepository productRepository;
	
	@Override
	public Product save(Product product) throws Exception {
		
		return productRepository.save(product); 
	}

	@Override
	public Product update(Product product) throws Exception {
		
    	Product newProduct = findById(product.getId());
    	if (newProduct != null) {
    		
    		newProduct.setName(product.getName());
    		newProduct.setPrice(product.getPrice());
    		newProduct.setQuantity(product.getQuantity());
    		return productRepository.save(newProduct);
    		 
    	} else {
    		throw new Exception("Produit  introuvable.");
    	}
	}

	@Override
	public List<Product> findAll() {
		List<Product> list = new ArrayList<>();
		productRepository.findAll().iterator().forEachRemaining(list::add);
		return list;
	}

	@Override
	public void delete(long id) {
		productRepository.deleteById(id);
	}

	@Override
	public Product findById(Long id) {
		// TODO Auto-generated method stub
		Product p;
		try {
			p = productRepository.findById(id).get();
		} catch(Exception e) {
			return null;
		}
		return p;
	}



	@Override
	public Product findByName(String name) {
		return productRepository.findByName(name);
	}

}
