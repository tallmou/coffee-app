package backend.service;

import java.util.List;

import backend.models.Machine;
import backend.models.User;


public interface MachineService {

    Machine save(Machine machine) throws Exception;
    
    Machine update(Machine machine) throws Exception;

    List<Machine> findAll();

    void delete(long id);

    Machine findByPort(int port);

    Machine findById(Long id);
    
    void assign(long machineId,long userId) throws Exception;
    
    boolean isUsedPort(int port);
    
    void addProduct(long machineId,long productId) throws Exception;
    
    void stockUpdate(long machineId,long productId, double quantity);
    
    List<Machine> findByOwner(User user);
}
