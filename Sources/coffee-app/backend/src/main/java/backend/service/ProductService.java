package backend.service;

import java.util.List;

import backend.models.Product;

public interface ProductService {

    Product save(Product product) throws Exception;
    
    Product update(Product product) throws Exception;

    List<Product> findAll();

    void delete(long id);


    Product findById(Long id);
    
    Product findByName(String name);
    
}
