package backend.service;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;


@Component
public class EmailServiceImpl implements EmailService {

  @Autowired
  public JavaMailSender emailSender;
  
  public static int noOfQuickServiceThreads = 20;
  
  private ScheduledExecutorService quickService = Executors.newScheduledThreadPool(noOfQuickServiceThreads);
  
  public void sendMessage(String htmlMsg, String subject, String mail) throws MailException,RuntimeException {
	    MimeMessage mimeMessage = emailSender.createMimeMessage();
		MimeMessageHelper helper;
		try {
			helper = new MimeMessageHelper(mimeMessage, false, "utf-8");
			mimeMessage.setContent(htmlMsg, "text/html; charset=utf-8");
			helper.setTo(mail);
			helper.setSubject(subject);
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		quickService.submit(new Runnable() {
			@Override
			public void run() {
				try{
					 emailSender.send(mimeMessage); 
				}catch(Exception e){
					e.printStackTrace();
				}
			}
		});
	   
  }


}
