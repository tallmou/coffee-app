package backend.service;

import org.springframework.mail.MailException;

public interface EmailService {

  // Send simple Email
  public void sendMessage(String message, String subject, String mail) throws MailException,RuntimeException;
}
