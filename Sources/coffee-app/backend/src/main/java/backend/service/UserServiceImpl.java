package backend.service;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import backend.models.LoginUser;
import backend.models.Role;
import backend.models.User;
import backend.repository.RoleRepository;
import backend.repository.UserRepository;
import backend.repository.VerificationTokenRepository;
import backend.utils.Util;


@Service(value = "userService")
public class UserServiceImpl implements UserDetailsService, UserService {

    @Autowired
    private UserRepository userDao;
    
    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private BCryptPasswordEncoder bcryptEncoder;
    
    @Autowired
	EmailServiceImpl emailService;

	@Autowired
	VerificationTokenRepository verificationTokenRepository;

    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userDao.findByEmail(username);
        if (user == null) {
            throw new UsernameNotFoundException("Invalid username or password.\n Username: " + username);
        }
        
        org.springframework.security.core.userdetails.User userT =  new org.springframework.security.core.userdetails.User(user.getEmail(), user.getPassword(), getAuthority(user.getRole()));
        return userT;
    }

    private List<SimpleGrantedAuthority> getAuthority(Role role) {
        return Arrays.asList(new SimpleGrantedAuthority(role.getName()));
    }

    public List<User> findAll() {
        List<User> list = new ArrayList<>();
        userDao.findAll().iterator().forEachRemaining(list::add);
        return list;
    }

    @Override
    public void delete(long id) {
        userDao.deleteById(id);
    }

    @Override
    public User findOne(String email) {
        return userDao.findByEmail(email);
    }

    @Override
    public User findById(Long id) {
        return userDao.findById(id).orElse(null);
    }
    
    @Override
    public User findByEmail(String email) {
        return userDao.findByEmail(email);
    }

    @Override
    public User save(User user) throws Exception {
		if (Util.isEmailValid(user.getEmail())) {
			String roleName = (user.getRole() == null) ? "ROLE_USER" : user.getRole().getName();
			Role r = roleRepository.findByName(roleName);
			user.setRole(r);
			return userDao.save(user);
			
		} else {
			throw new Exception("email invalid");
		}      
    }
    
    @Override
    public User update(User user) throws Exception {
    	User newUser = findById(user.getId());
    	if (newUser != null) {
    		if (Util.isEmailValid(user.getEmail())) {
    			newUser.setEmail(user.getEmail());
    			newUser.setFirstName(user.getFirstName());
    			newUser.setLastName(user.getLastName());
    			return userDao.save(newUser);

    		} else {
    			throw new Exception("email invalid");
    		} 
    	} else {
    		throw new Exception("Utilisateur introuvable");
    	}
		     
    }

	public void activateAccount(LoginUser loginUser) throws Exception {
		// TODO Auto-generated method stub
		User user = findByEmail(loginUser.getEmail());
		if(user == null) {
			throw new Exception("Utilisateur inexistant");
		}
		if (Util.isPasswordValid(loginUser.getPassword())) {
			user.setActive(true);
			user.setPassword(bcryptEncoder.encode(loginUser.getPassword()));
			userDao.save(user);
		} else {
			throw new Exception("password invalide");
		}

	}

	public void changePassword(LoginUser loginUser) throws Exception {
		// TODO Auto-generated method stub
		User user = findByEmail(loginUser.getEmail());
		if(user == null) {
			throw new Exception("Utilisateur inexistant");
		}
		if (!user.isActive()) {
			throw new Exception("compte inactif");
		}
		if (Util.isPasswordValid(loginUser.getPassword())) {
			user.setPassword(bcryptEncoder.encode(loginUser.getPassword()));
			userDao.save(user);
		} else {
			throw new Exception("password invalide");
		}
	}
	
	public Collection<? extends GrantedAuthority> getAuthorities() {
	    List<SimpleGrantedAuthority> authorities = new ArrayList<SimpleGrantedAuthority>();

	    /* this is just a sample , you must implement bringing data from repository then prefix every string with ROLE_ */
	    authorities.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
	    authorities.add(new SimpleGrantedAuthority("ROLE_USER"));

	    return authorities;
	}
}
