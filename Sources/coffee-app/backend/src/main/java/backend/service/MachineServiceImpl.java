package backend.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import backend.models.Machine;
import backend.models.Product;
import backend.models.User;
import backend.repository.MachineRepository;
import backend.repository.ProductRepository;
import backend.repository.UserRepository;

@Service(value = "machineService")
public class MachineServiceImpl implements MachineService {

	@Autowired
	private MachineRepository machineRepository;

	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private ProductRepository productRepository;
	
	@Override
	public Machine save(Machine machine) throws Exception {

		if (isUsedPort(machine.getPort())) throw new Exception("Ce port est déjà utilisé par une autre machine.");
		return machineRepository.save(machine); 

	}

	@Override
	public Machine update(Machine machine) throws Exception {
		Machine newMachine = findById(machine.getId());
		if (newMachine != null) {
			if (isUsedPort(machine.getPort())) {
				newMachine.setName(machine.getName());
				newMachine.setPort(machine.getPort());
				newMachine.setFrequency(machine.getFrequency());
				newMachine.setVersion(machine.getVersion());
				newMachine.setUrl(machine.getUrl());
				return machineRepository.save(newMachine);

			} else {
				throw new Exception("Ce port est déjà utilisé par une autre machine.");
			} 
		} else {
			throw new Exception("Machine introuvable.");
		}
	}

	@Override
	public List<Machine> findAll() {
		List<Machine> list = new ArrayList<>();
		machineRepository.findAll().iterator().forEachRemaining(list::add);
		return list;
	}

	@Override
	public void delete(long id) {

		machineRepository.deleteById(id);
	}

	@Override
	public Machine findByPort(int port) {

		return machineRepository.findByPort(port);
	}

	@Override
	public Machine findById(Long id) {
		Optional<Machine> machine=machineRepository.findById(id);

		return machine.get();
	} 

	@Override
	public boolean isUsedPort(int port) {
		Machine machine=machineRepository.findByPort(port);

		return machine!=null;
	}

	@Override
	public void assign(long machineId, long userId) throws Exception {
		Machine machine= machineRepository.findById(machineId).orElse(null);
		User user= userRepository.findById(userId).orElse(null);
		if(machine == null) 
			throw new Exception("Machine introuvable.");
		if(user == null) 
			throw new Exception("Utilisateur introuvable.");

		machine.setOwner(user);
		user.getMachines().add(machine);
		machineRepository.save(machine);
		userRepository.save(user);
	}

	@Override
	public void addProduct(long machineId, long productId) throws Exception {
		
		Machine machine= machineRepository.findById(machineId).orElse(null);
		Product product= productRepository.findById(productId).orElse(null);

		if(machine==null) throw new Exception("Machine introuvable.");
		if(product==null) throw new Exception("Produit introuvable.");
		
		Product productInMachine = machine
									.getProducts()
									.stream()
									.filter(p-> p.getName()!= product.getName())
									.findFirst()
									.orElse(null);
		if (productInMachine != null) throw new Exception("Un produit du meme nom existe déjà.");
		
		product.setMachine(machine);
		machine.getProducts().add(product);
		
		productRepository.save(product);
		machineRepository.save(machine);
		
	}

	@Override
	public void stockUpdate(long machineId, long productId,double quantity) {
		Machine machine= machineRepository.findById(machineId).orElse(null);
		
		if( machine!= null) {
			machine.getProducts()
			.stream()
			.filter( product -> product.getId() == productId )
			.findFirst()
			.get()
			.setQuantity(quantity);
		}
		
	}
	
	public List<Machine> findByOwner(User user) {
		// TODO Auto-generated method stub
		return machineRepository.findByOwner(user);
	}



}
