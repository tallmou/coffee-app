package backend.utils;


import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.Arrays;
import java.util.List;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import backend.models.Product;
import backend.models.User;

public class PDFTransformer {


    public static ByteArrayInputStream usersExport(List<User> users) {

        Document document = new Document();
        ByteArrayOutputStream out = new ByteArrayOutputStream();

        try {
        	List<String> headers = Arrays.asList("Id", "Nom", "Prénom", "Email", "Active");
        	
            PdfPTable table = new PdfPTable(headers.size());
            table.setWidthPercentage(90);
            table.setWidths(new int[]{1, 2, 3, 4, 1});

            Font headFont = FontFactory.getFont(FontFactory.HELVETICA_BOLD);
            
           
            for (String h : headers) {
            	PdfPCell hcell = new PdfPCell(new Phrase(h, headFont));
                hcell.setBackgroundColor(BaseColor.LIGHT_GRAY);
                hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                hcell.setBorderWidth(2);
                table.addCell(hcell);
            }

           

            for (User user : users) {

                PdfPCell cell;

                cell = new PdfPCell(new Phrase(String.valueOf(user.getId())));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(cell);

                cell = new PdfPCell(new Phrase(user.getLastName()));
                cell.setPaddingLeft(5);
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                table.addCell(cell);
                
                cell = new PdfPCell(new Phrase(user.getFirstName()));
                cell.setPaddingLeft(5);
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                table.addCell(cell);
                
                cell = new PdfPCell(new Phrase(user.getEmail()));
                cell.setPaddingLeft(5);
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                table.addCell(cell);
                
                String active = (user.isActive()) ? "OUI" : "NON";
                cell = new PdfPCell(new Phrase(active));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setHorizontalAlignment(Element.ALIGN_MIDDLE);
                cell.setPaddingRight(5);
                table.addCell(cell);
            }

            PdfWriter.getInstance(document, out);
            document.open();
            Font font = FontFactory.getFont(FontFactory.COURIER, 14, BaseColor.BLACK);
            Paragraph para = new Paragraph( "LISTE DES UTILISATEURS", font);
            para.setAlignment(Element.ALIGN_CENTER);
            document.add(para);
            document.add(Chunk.NEWLINE);
            document.add(table);

            document.close();

        } catch (DocumentException ex) {

            System.err.println(ex.getMessage());
        }

        return new ByteArrayInputStream(out.toByteArray());
    }
    
    public static ByteArrayInputStream productsExport(List<Product> products) {

        Document document = new Document();
        ByteArrayOutputStream out = new ByteArrayOutputStream();

        try {
        	List<String> headers = Arrays.asList("Id", "Nom","Machine", "Quantité", "Prix");
        	
            PdfPTable table = new PdfPTable(headers.size());
            table.setWidthPercentage(90);
            table.setWidths(new int[]{1, 3, 3, 1, 1});

            Font headFont = FontFactory.getFont(FontFactory.HELVETICA_BOLD);
            
           
            for (String h : headers) {
            	PdfPCell hcell = new PdfPCell(new Phrase(h, headFont));
                hcell.setBackgroundColor(BaseColor.LIGHT_GRAY);
                hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                hcell.setBorderWidth(2);
                table.addCell(hcell);
            }

           

            for (Product product : products) {

                PdfPCell cell;

                cell = new PdfPCell(new Phrase(String.valueOf(product.getId())));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(cell);

                cell = new PdfPCell(new Phrase(product.getName()));
                cell.setPaddingLeft(5);
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                table.addCell(cell);
                
                cell = new PdfPCell(new Phrase(product.getMachine().getName()));
                cell.setPaddingLeft(5);
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                table.addCell(cell);
                
                cell = new PdfPCell(new Phrase(String.valueOf(product.getQuantity())));
                cell.setPaddingLeft(5);
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                table.addCell(cell);
                
                cell = new PdfPCell(new Phrase(String.valueOf(product.getPrice())));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setHorizontalAlignment(Element.ALIGN_MIDDLE);
                cell.setPaddingRight(5);
                table.addCell(cell);
            }

            PdfWriter.getInstance(document, out);
            document.open();
            Font font = FontFactory.getFont(FontFactory.COURIER, 14, BaseColor.BLACK);
            Paragraph para = new Paragraph( "LISTE DES PRODUITS", font);
            para.setAlignment(Element.ALIGN_CENTER);
            document.add(para);
            document.add(Chunk.NEWLINE);
            document.add(table);

            document.close();

        } catch (DocumentException ex) {

            System.err.println(ex.getMessage());
        }

        return new ByteArrayInputStream(out.toByteArray());
    }
}
