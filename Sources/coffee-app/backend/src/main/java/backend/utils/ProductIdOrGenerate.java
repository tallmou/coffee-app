package backend.utils;

import java.io.Serializable;

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.id.IdentityGenerator;

import backend.models.Product;

public class ProductIdOrGenerate extends IdentityGenerator{
	
	@Override
	public Serializable generate(SharedSessionContractImplementor s, Object obj) {
		// TODO Auto-generated method stub
		System.err.println("IDGENERATOR");
		if (obj == null) throw new HibernateException(new NullPointerException()) ;

		if ((((Product) obj).getId()) == null) {
			Serializable id = super.generate(s, obj) ;
			return id;
		} else {
			return ((Product) obj).getId();
		}
	}
	

}