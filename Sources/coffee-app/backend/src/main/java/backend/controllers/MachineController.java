package backend.controllers;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.hibernate.exception.ConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import backend.models.ApiResponse;
import backend.models.Machine;
import backend.models.MachineDTO;
import backend.models.User;
import backend.service.MachineServiceImpl;
import backend.service.UserServiceImpl;


@RestController
@RequestMapping("/api/machines")
@CrossOrigin(origins = "*")
public class MachineController {
	
	Logger logger = LoggerFactory.getLogger(MachineController.class);

	@Autowired 
	private MachineServiceImpl machineService;

	@Autowired 
	private UserServiceImpl userService;

	@Autowired
	private ObjectMapper objectMapper = new ObjectMapper().enable(SerializationFeature.INDENT_OUTPUT);

	@GetMapping
	@PreAuthorize("isAuthenticated()")
	public String getMachines() throws JsonProcessingException {
		String currentEmail = SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString();
		User user = userService.findByEmail(currentEmail);
		if (user.getRole().getName().equals("ROLE_ADMIN")) {
			return objectMapper.writeValueAsString(machineService.findAll());
		}
		
		return objectMapper.writeValueAsString(machineService.findByOwner(user));
	}

	@PostMapping
	@PreAuthorize("hasRole('ADMIN')")
	public String createMachine(@RequestBody MachineDTO machineDTO) throws JsonProcessingException {
		try {
			User user=userService.findById(machineDTO.getUserId());
			Machine machine = new Machine(machineDTO.getUrl(),
											machineDTO.getPort(),
											machineDTO.getName(),
											machineDTO.getFrequency(),
											machineDTO.getVersion(),
											user);
			
			machine = machineService.save(machine);
			return objectMapper.writeValueAsString(
					new ApiResponse(HttpStatus.OK,
							"Machine Ajoutée. Identifiant :" + machine.getId())
					);
		} catch (ConstraintViolationException ex) {
			return objectMapper.writeValueAsString(
					new ApiResponse(HttpStatus.IM_USED,
							"Erreur lors de l'ajout de la machine : Le port "+machineDTO.getPort()+" est utilisé par une autre machine")
					);
		}	
		catch(Exception e) {
			return objectMapper.writeValueAsString(
					new ApiResponse(HttpStatus.BAD_REQUEST,
							"Erreur lors de l'ajout de la machine : " + e.getMessage())
					);
		}
	}

	@PutMapping("/{id}")
	@PreAuthorize("isAuthenticated()")
	public String configMachine(HttpServletResponse response, @PathVariable Long id, @RequestBody MachineDTO machineDTO) throws IOException {
		System.out.println("\n\nHey");
		Machine machine = new Machine(machineDTO.getId(), 
				machineDTO.getPort(),
				machineDTO.getName(),
				machineDTO.getFrequency(),
				machineDTO.getVersion()
				);
		machine.setUrl(machineDTO.getUrl());
		String currentEmail = SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString();
		User user = userService.findByEmail(currentEmail);
		if (user==null || (!user.getRole().getName().equals("ROLE_ADMIN") && user.getId()!= machineDTO.getUserId())) {		
			response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Cet utilisateur n'a pas le droit de modifier cette machine.");
		}
		try {
			machine.setId(id);
			machine = machineService.update(machine);
			return objectMapper.writeValueAsString(
					new ApiResponse(HttpStatus.OK,
							"Configuration de la machine modifiée avec succès.")
					);
		} catch (ConstraintViolationException ex) {
			ex.printStackTrace();
			return objectMapper.writeValueAsString(
					new ApiResponse(HttpStatus.IM_USED,
							"Erreur lors de la modification  : Le port "+machine.getPort()+" est déjà utilisé")
					); 
		}	
		catch(Exception e) {
			e.printStackTrace();
			return objectMapper.writeValueAsString(
					new ApiResponse(HttpStatus.BAD_REQUEST,
							"Erreur lors de la modification de la machine : " + e.getMessage())
					);
		}
	}

	@DeleteMapping("/{id}")
	@PreAuthorize("hasRole('ADMIN')")
	public String deleteMachine(@PathVariable Long id) throws JsonProcessingException {
		machineService.delete(id);
		return objectMapper.writeValueAsString(
				new ApiResponse(HttpStatus.OK,
						"Machine supprimée avec succès. ")
				);
	}

	@GetMapping("/{id}")
	@PreAuthorize("isAuthenticated()")
	public Machine getMachine(HttpServletResponse response, @PathVariable Long id) throws IOException {
		Machine machine=machineService.findById(id);

		String currentEmail = SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString();
		User user = userService.findByEmail(currentEmail);
		if (user == null || (!user.getRole().getName().equals("ROLE_ADMIN") && user.getId()!=machine.getOwner().getId())) {
			response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Cet utilisateur n'a pas le droit d'afficher ce machine.");
		}
		return machine;
	}

	@PutMapping("/assign/{id}")
	@PreAuthorize("hasRole('ADMIN')")
	public String assignUser(@PathVariable long id, @RequestBody long userId) throws Exception {
		try {
			machineService.assign(id, userId);
			return objectMapper.writeValueAsString(
					new ApiResponse(HttpStatus.OK,
							"La machine "+id+" a bien été assigné à l'utilisateur "+userId+".")
					);
		} catch (Exception e) {
			return objectMapper.writeValueAsString(
					new ApiResponse(HttpStatus.BAD_REQUEST,
							"Erreur lors de l'assignation de la machine : " + e.getMessage() + " "+e.getClass())
					);
		}

	}
	

	


}
