package backend.controllers;

import java.util.Arrays;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import backend.config.JwtTokenUtil;
import backend.models.ApiResponse;
import backend.models.AuthToken;
import backend.models.LoginUser;
import backend.models.User;
import backend.service.UserServiceImpl;

@RestController
@RequestMapping("/api/auth")
public class AuthController {
	@Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private UserServiceImpl userService;
    
    @Autowired
    private ObjectMapper objectMapper = new ObjectMapper().enable(SerializationFeature.INDENT_OUTPUT);
    
    @CrossOrigin
    @PostMapping("/login")
    public ResponseEntity<?> register(@RequestBody LoginUser loginUser) throws AuthenticationException, com.fasterxml.jackson.core.JsonProcessingException {
      try {
    	final User user = userService.findOne(loginUser.getEmail());
    	
    	Authentication authentication = authenticationManager.authenticate(
              new UsernamePasswordAuthenticationToken(
                      loginUser.getEmail(),
                      loginUser.getPassword(),
                      Arrays.asList(new SimpleGrantedAuthority(user.getRole().getName()))
              )
        );
    	
    	
    	if(!user.isActive()) {
    		  return new ResponseEntity<String>(
                      objectMapper.writeValueAsString(
                          new ApiResponse(HttpStatus.LOCKED,
                                          "Compte utilisateur non activé !")
                      ), HttpStatus.OK);
    	}
    	
    	
    	SecurityContextHolder.getContext().setAuthentication(authentication);
        
		final String token = jwtTokenUtil.generateToken(user);
		return new ResponseEntity<String>(
                objectMapper.writeValueAsString(
                    new ApiResponse(HttpStatus.OK,
                                      objectMapper.writeValueAsString(new AuthToken(token, user)))
                ), HttpStatus.OK);
      } catch(BadCredentialsException e) {
          return new ResponseEntity<String>(
                  objectMapper.writeValueAsString(
                      new ApiResponse(HttpStatus.BAD_REQUEST,
                                      "E-Mail / Mot de passe invalide")
                  ), HttpStatus.OK);
        } catch(AuthenticationException e) {
      	  e.printStackTrace();
          return new ResponseEntity<String>(
                  objectMapper.writeValueAsString(
                      new ApiResponse(HttpStatus.BAD_REQUEST,
                                      "Authentication Exception",
                                      e)
                  ), HttpStatus.OK);
        }
    }
    
    
    @CrossOrigin
    @GetMapping("/logout")
    public ResponseEntity<?> logoutPage (HttpServletRequest request, HttpServletResponse response) throws JsonProcessingException {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        
        if (auth != null){   
            new SecurityContextLogoutHandler().logout(request, response, auth);

        } 
        return new ResponseEntity<String>(
                objectMapper.writeValueAsString(
                    new ApiResponse(HttpStatus.OK,"Déconnexion réussite")
                ), HttpStatus.OK);
    }
    
    @GetMapping("/username")
    public String currentUserName(HttpServletRequest request) {
    	if(request.getUserPrincipal() != null) {
            String loginName = request.getUserPrincipal().getName();
           return "loginName : " + loginName ;
        }
    	return "NONE";
    }
}
