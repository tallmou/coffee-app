package backend.controllers;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import backend.models.ApiResponse;
import backend.models.Machine;
import backend.models.Product;
import backend.models.ProductDTO;
import backend.models.SynchronizationDTO;
import backend.models.User;
import backend.service.MachineServiceImpl;
import backend.service.ProductServiceImpl;
import backend.service.RestService;
import backend.service.UserServiceImpl;
import backend.utils.PDFTransformer;

@RestController
@RequestMapping("/api/products")
@CrossOrigin(origins = "*")
public class ProductController {

	@Autowired 
	private ProductServiceImpl productService;
	
	@Autowired 
	private RestService restService;
	
	@Autowired 
	private MachineServiceImpl machineService;
	
	@Autowired 
	private UserServiceImpl userService;
	
	@Autowired
	private ObjectMapper objectMapper = new ObjectMapper().enable(SerializationFeature.INDENT_OUTPUT);

	@GetMapping
	@PreAuthorize("isAuthenticated()")
	public String getProducts() throws JsonProcessingException {
		String currentEmail = SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString();
		User user = userService.findByEmail(currentEmail);
		if (user.getRole().getName().equals("ROLE_ADMIN")) {
			return objectMapper.writeValueAsString(productService.findAll());
		}
		return objectMapper.writeValueAsString(productService.findAll()
				.stream()
				.filter(p -> p.getMachine().getOwner().getId() == user.getId())
				.collect(Collectors.toList()));
		
	}

	@PostMapping
	@PreAuthorize("hasRole('USER')")
	public String createProduct(@RequestBody ProductDTO productDTO) throws JsonProcessingException {
		
		Machine machine = machineService.findById(productDTO.getMachineId());
		
		if (machine == null) return objectMapper.writeValueAsString(
				new ApiResponse(HttpStatus.BAD_REQUEST,
						"Aucune machine ne porte cet Id : ")
				);
		
		String currentEmail = SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString();
		User user = userService.findByEmail(currentEmail);
		
		if (user == null || (user.getId()!=machine.getOwner().getId())) {
			// Personaliser ce retour
			return null;
		}

		try {
			Product product = new Product (productDTO.getName(),productDTO.getQuantity(),productDTO.getPrice(),machine);
			product = productService.save(product);
			productDTO.setId(product.getId());
			String url = machine.getUrl() + ":" + machine.getPort() + "/api/products";
			restService.addProduct(url, productDTO);
			return objectMapper.writeValueAsString(
					new ApiResponse(HttpStatus.OK,
							"Produit Ajouté à la machine. IdProduit :" + product.getId() +", IdMachine :"  + machine.getId())
					);
			
		} 	
		catch(Exception e) {
			return objectMapper.writeValueAsString(
					new ApiResponse(HttpStatus.BAD_REQUEST,
							"Erreur lors de l'ajout du produit : " + e.getMessage())
					);
		}
	}
	
	@PostMapping("/synchronize")
	public String syncProducts(@RequestBody SynchronizationDTO productSync) throws JsonProcessingException {
		System.out.println("\n\nsynchro");
		Machine m = machineService.findById(productSync.getMachineId());
		if(m == null) {
			return "Machine inextistante";
		}
		
		System.out.println("machine " + productSync.getMachineId());
		List<Product> products = productService.findAll()
				.stream()
				.filter(p -> p.getMachine().getId() == productSync.getMachineId())
				.collect(Collectors.toList());
		
	
		for (Product p : products) {
			boolean find = false;
			for (Product p1 : productSync.getProducts()) {
				if(p1.getId() == p.getId()) {
					p1.setMachine(m);
					try {
						productService.update(p1);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					find = true; 
					break;
				} 
			}
			if (!find) {
				System.err.println("DELeTE");
				productService.delete(p.getId());
			}
		}
		
		for (Product p : productSync.getProducts()) {
			if(productService.findById(p.getId()) == null) {
				System.err.println("Not Found");
				p.setMachine(m);
				try {
					System.out.println("idDT " + p.getId());
					Product p2 = productService.save(p);
					System.out.println("id =" + p2.getId());
				} catch (Exception e) {
					// TODO Auto-generated catch block
					//e.printStackTrace();
				}
			} else {
				System.err.println("Found");
			}
			
		}
		return "OK";
	}

	@PutMapping("/{id}")
	@PreAuthorize("hasRole('USER')")
	public String updateProduct(HttpServletRequest req,@PathVariable Long id, @RequestBody ProductDTO productDTO) throws JsonProcessingException {

		try {		
			Machine machine = machineService.findById(productDTO.getMachineId());
			String currentEmail = SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString();
			User user = userService.findByEmail(currentEmail);
	
			if (user == null || (user.getId()!=machine.getOwner().getId())) {
				// Personaliser ce retour
				return null;
			}
			Product product = new Product (productDTO.getName(),productDTO.getQuantity(),productDTO.getPrice(),machine);
			product.setId(id);
			product = productService.update(product);
			productDTO.setId(product.getId());
			String url = machine.getUrl() + ":" + machine.getPort() + "/api/products/" + id;
			restService.updateProduct(url, productDTO);
			return objectMapper.writeValueAsString(
					new ApiResponse(HttpStatus.OK,
							"Produit modifié.")
					);
		} 	
		catch(Exception e) {
			e.printStackTrace();
			return objectMapper.writeValueAsString(
					new ApiResponse(HttpStatus.BAD_REQUEST,
							"Erreur lors de la modification du produit : " + e.getMessage())
					);
		}
	}

	@DeleteMapping("/{id}")
	@PreAuthorize("hasRole('USER')")
	public String deleteProduct(@PathVariable Long id) throws JsonProcessingException {
		
		Product product = productService.findById(id);
		Machine machine = machineService.findById(product.getMachine().getId());
		String currentEmail = SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString();
		User user = userService.findByEmail(currentEmail);

		if (user == null || (user.getId()!=machine.getOwner().getId())) {
			// Personaliser ce retour
			return null;
		}
		productService.delete(id);
		String urlM = machine.getUrl().trim();
		
		String url = urlM + ":" + machine.getPort() + "/api/products/" + id;

		restService.deleteProduct(url.trim());
		return objectMapper.writeValueAsString(
				new ApiResponse(HttpStatus.OK,
						"Produit supprimée avec succès. ")
				);
	}

	@GetMapping("/{id}")
	@PreAuthorize("isAuthenticated()")
	public Product getProduct(HttpServletRequest req,@PathVariable Long id) {
		
		Product product = productService.findById(id);
		Machine machine = machineService.findById(product.getMachine().getId());
		String currentEmail = SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString();
		User user = userService.findByEmail(currentEmail);
		if (!user.getRole().getName().equals("ROLE_ADMIN") && 
			(user == null || (user.getId()!=machine.getOwner().getId()))) {
			// Personaliser ce retour
			return null;
		}
		
		return product;
	}
	
	@GetMapping("/export")
	@PreAuthorize("hasRole('ADMIN')")
	@ResponseBody
	public String getDocument(HttpServletRequest request, HttpServletResponse response)
			throws com.fasterxml.jackson.core.JsonProcessingException {

        ByteArrayInputStream bis = PDFTransformer.productsExport(productService.findAll());
        String fileName ="products.pdf";
        String mimeType = "application/pdf";
		response.setContentType(mimeType);

		response.setHeader("Content-Disposition", String.format("inline; filename=\"" + fileName + "\""));
		response.setContentLength(new byte[bis.available()].length);
		

		try {

			FileCopyUtils.copy(bis, response.getOutputStream());

			return objectMapper.writeValueAsString(
					new ApiResponse(HttpStatus.OK,
							"OK")
					);

		} catch (IOException e) {
			return objectMapper.writeValueAsString(
					new ApiResponse(HttpStatus.EXPECTATION_FAILED,
							"Error while copying stream",
							e)
					);
		}
	}


}
