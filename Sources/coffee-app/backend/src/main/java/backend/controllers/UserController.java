package backend.controllers;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.exception.ConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import backend.models.ApiResponse;
import backend.models.LoginUser;
import backend.models.User;
import backend.models.VerificationToken;
import backend.repository.VerificationTokenRepository;
import backend.service.EmailServiceImpl;
import backend.service.UserServiceImpl;
import backend.utils.PDFTransformer;

@RestController
@RequestMapping("/api/users")
@CrossOrigin(origins = "*")
public class UserController {

	Logger logger = LoggerFactory.getLogger(UserController.class);

	@Autowired 
	private UserServiceImpl userService;

	@Autowired
	EmailServiceImpl emailService;

	@Autowired
	VerificationTokenRepository verificationTokenRepository;

	@Autowired
	private ObjectMapper objectMapper = new ObjectMapper().enable(SerializationFeature.INDENT_OUTPUT);

	@GetMapping
	@PreAuthorize("hasRole('ADMIN')")
	public String getUsers() throws JsonProcessingException {
		return objectMapper.writeValueAsString(userService.findAll()
				.stream()
				.filter(u -> u.getRole().getName().contentEquals("ROLE_USER"))
				.collect(Collectors.toList()));
	}

	@PostMapping
	@PreAuthorize("hasRole('ADMIN')")
	public String createUser(HttpServletRequest request, @RequestBody User user) throws JsonProcessingException {
		try {
			user = userService.save(user);
			String port = (request.getServerPort() == 80) ? "" : ":" + request.getServerPort();
			String path = (request.getServerPort() == 80) ? "" : "";
			VerificationToken verificationToken = new VerificationToken(user);
			verificationTokenRepository.save(verificationToken);
			String url = String.format("%s://%s%s%s/activate/%s/%s",
					request.getScheme(),
					request.getServerName(),
					path,
					port,
					user.getEmail(),
					verificationToken.getToken());
			
			String htmlMsg = "Vous avez été inscrit dans la plateforme de gestion des machines à café Coffee-APP.\n"
		    		+ "Cliquez sur ce <a href=\"" + url + "\">lien</a> pour choisir votre mot de passe et accèder à votre compte !";
			
		    String subject = "Inscription à la plateforme Coffee-APP";
			emailService.sendMessage(htmlMsg, subject, user.getEmail());
			return objectMapper.writeValueAsString(
					new ApiResponse(HttpStatus.OK,
							"Utilisateur créé avec succès. Identifiant :" + user.getId())
					);
		} catch (ConstraintViolationException ex) {
			return objectMapper.writeValueAsString(
					new ApiResponse(HttpStatus.IM_USED,
							"Erreur lors de la création de l'utilisateur : Email déjà existant")
					);
		}	
		catch(Exception e) {
			e.printStackTrace();
			return objectMapper.writeValueAsString(
					new ApiResponse(HttpStatus.BAD_REQUEST,
							"Erreur lors de la création de l'utilisateur : " + e.getMessage())
					);
		}
	}

	@PutMapping("/{id}")
	@PreAuthorize("hasRole('USER')")
	public String updateUser(HttpServletResponse response, @PathVariable Long id, @RequestBody User user) throws IOException {
		String currentEmail = SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString();
		User currentUser = userService.findByEmail(currentEmail);
		if (currentUser == null || (!currentUser.getRole().getName().equals("ROLE_ADMIN") && id != currentUser.getId())) {
			
			response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Unauthorized user");
			return objectMapper.writeValueAsString(
					new ApiResponse(HttpStatus.BAD_REQUEST,
							"Cet utilisateur n'a pas le droit de modifier cette .")
					);
		}
		try {
			user.setId(id);
			user = userService.update(user);
			return objectMapper.writeValueAsString(
					new ApiResponse(HttpStatus.OK,
							"Utilisateur modifié avec succès.")
					);
		} catch (ConstraintViolationException ex) {
			ex.printStackTrace();
			return objectMapper.writeValueAsString(
					new ApiResponse(HttpStatus.IM_USED,
							"Erreur lors de la modification de l'utilisateur : Email déjà existant")
					); 
		}	
		catch(Exception e) {
			e.printStackTrace();
			return objectMapper.writeValueAsString(
					new ApiResponse(HttpStatus.BAD_REQUEST,
							"Erreur lors de la modification de l'utilisateur : " + e.getMessage())
					);
		}
	}

	@DeleteMapping("/{id}")
	@PreAuthorize("hasRole('ADMIN')")
	public String deleteUser(@PathVariable Long id) throws JsonProcessingException {
		userService.delete(id);
		return objectMapper.writeValueAsString(
				new ApiResponse(HttpStatus.OK,
						"Utilisateur supprimmé avec succès. ")
				);
	}

	@GetMapping("/{id}")
	@PreAuthorize("isAuthenticated()")
	public User getUser(HttpServletResponse response, @PathVariable Long id) throws IOException {
		String currentEmail = SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString();
		User currentUser = userService.findByEmail(currentEmail);
		if (currentUser == null || (!currentUser.getRole().getName().equals("ROLE_ADMIN") && id != currentUser.getId())) {
			response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Unauthorized user");
		}
		return userService.findById(id);
	}
	
	@GetMapping("/password_forgotten")
	public String PasswordForgotten(HttpServletRequest request, @RequestParam String email) throws JsonProcessingException {
		User user = userService.findByEmail(email.trim());
		if(user == null) {
			return objectMapper.writeValueAsString(
					new ApiResponse(HttpStatus.EXPECTATION_FAILED,
							"Utilisateur inexistant " + email)
					);
		}
		String port = (request.getServerPort() == 80) ? "" : ":" + request.getServerPort();
		String path = (request.getServerPort() == 80) ? "" : "";
		VerificationToken verificationToken = new VerificationToken(user);
		verificationTokenRepository.save(verificationToken);
		String url = String.format("%s://%s%s%s/change_password/forgotten/%s/%s",
				request.getScheme(),
				request.getServerName(),
				path,
				port,
				user.getEmail(),
				verificationToken.getToken());
		String htmlMsg = "Vous nous avez signalé avoir oublié votre mot de passe \n"
	    		+ "Cliquez sur ce <a href=\"" + url + "\">lien</a> pour choisir un nouveau mot de passe";
		
	    String subject = "Inscription à la plateforme Coffee-APP";
		emailService.sendMessage(htmlMsg, subject, user.getEmail());
		return objectMapper.writeValueAsString(
				new ApiResponse(HttpStatus.OK,
						"Un lien a été envoyé par email !")
				);
	}
	
	
	@PutMapping("/change_password/forgotten/{token}")
	public String changeForgotPassword(@PathVariable String token, @RequestBody LoginUser loginUser) throws JsonProcessingException {
		if (token == null) {
			return objectMapper.writeValueAsString(
					new ApiResponse(HttpStatus.EXPECTATION_FAILED,
							"Il manque le token d'activation")
					);
		}

		VerificationToken verificationToken = verificationTokenRepository.findByToken(token);

		if (verificationToken == null) {
			return objectMapper.writeValueAsString(
					new ApiResponse(HttpStatus.EXPECTATION_FAILED,
							"Token invalide")
					);
		}

		Calendar cal = Calendar.getInstance();
		if ((verificationToken.getExpiryDate().getTime() - cal.getTime().getTime()) <= 0) {
			return objectMapper.writeValueAsString(
					new ApiResponse(HttpStatus.EXPECTATION_FAILED,
							"Le lien d'activation a expiré.")
					);
		}
		
		try {
			userService.changePassword(loginUser);
			return objectMapper.writeValueAsString(
					new ApiResponse(HttpStatus.OK,
							"Compte activé et mot de passe ajouté !")
					);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			return objectMapper.writeValueAsString(new ApiResponse(HttpStatus.BAD_REQUEST,
					"Erreur lors de la modification du mot de passe : " + e.getMessage())
			);
		}
		
		
	}
	
	@PutMapping("/change_password")
	@PreAuthorize("isAuthenticated()")
	public String changePassword(HttpServletResponse response, @RequestBody LoginUser loginUser) throws IOException {
		String currentEmail = SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString();
		User currentUser = userService.findByEmail(currentEmail);
		if (currentUser == null || (!currentUser.getRole().getName().equals("ROLE_ADMIN") && !currentUser.getEmail().equals(loginUser.getEmail()))) {
			response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Unauthorized user");
		}
		try {
			userService.changePassword(loginUser);
			return objectMapper.writeValueAsString(
					new ApiResponse(HttpStatus.OK,
							"Compte activé et mot de passe ajouté !")
					);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			return objectMapper.writeValueAsString(new ApiResponse(HttpStatus.BAD_REQUEST,
					"Erreur lors de la modification du mot de passe : " + e.getMessage())
			);
		}
		
		
	}
	
	
	@PutMapping("/activate/{token}")
	public String activateAccount(@PathVariable String token, @RequestBody LoginUser loginUser) throws JsonProcessingException {
		if (token == null) {
			return objectMapper.writeValueAsString(
					new ApiResponse(HttpStatus.EXPECTATION_FAILED,
							"Il manque le token d'activation")
					);
		}

		VerificationToken verificationToken = verificationTokenRepository.findByToken(token);

		if (verificationToken == null) {
			return objectMapper.writeValueAsString(
					new ApiResponse(HttpStatus.EXPECTATION_FAILED,
							"Token invalide")
					);
		}

		Calendar cal = Calendar.getInstance();
		if ((verificationToken.getExpiryDate().getTime() - cal.getTime().getTime()) <= 0) {
			return objectMapper.writeValueAsString(
					new ApiResponse(HttpStatus.EXPECTATION_FAILED,
							"Le lien d'activation a expiré.")
					);
		}
		
		try {
			userService.activateAccount(loginUser);
			return objectMapper.writeValueAsString(
					new ApiResponse(HttpStatus.OK,
							"Compte activé et mot de passe ajouté !")
					);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			return objectMapper.writeValueAsString(new ApiResponse(HttpStatus.BAD_REQUEST,
					"Erreur lors de la modification du mot de passe : " + e.getMessage())
			);
		}
		
		
	}
	
	@GetMapping("/export")
	@PreAuthorize("hasRole('ADMIN')")
	@ResponseBody
	public String getDocument(HttpServletRequest request, HttpServletResponse response)
			throws com.fasterxml.jackson.core.JsonProcessingException {

        ByteArrayInputStream bis = PDFTransformer.usersExport(userService.findAll());
        String fileName ="utilisateurs.pdf";
        String mimeType = "application/pdf";
		response.setContentType(mimeType);

		response.setHeader("Content-Disposition", String.format("inline; filename=\"" + fileName + "\""));
		response.setContentLength(new byte[bis.available()].length);
		

		try {

			FileCopyUtils.copy(bis, response.getOutputStream());

			return objectMapper.writeValueAsString(
					new ApiResponse(HttpStatus.OK,
							"OK")
					);

		} catch (IOException e) {
			return objectMapper.writeValueAsString(
					new ApiResponse(HttpStatus.EXPECTATION_FAILED,
							"Error while copying stream",
							e)
					);
		}
	}
	
}
