package backend.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import backend.models.Role;
import backend.models.User;
import backend.repository.RoleRepository;
import backend.service.UserServiceImpl;

@Component
public class InitialDataLoader implements ApplicationListener<ContextRefreshedEvent>{
	private boolean alreadySetup = false;
	private String email = "admin@gmail.com";
	private String password = "P@sser1234";
	 
    @Autowired
    private UserServiceImpl userService;
  
    @Autowired
    private RoleRepository roleRepository;
    
    @Autowired
    private BCryptPasswordEncoder bcryptEncoder;
    
    @Override
    @Transactional
    public void onApplicationEvent(ContextRefreshedEvent event) {
    	if (alreadySetup)
            return;
    	Role roleAdmin = createRoleIfNotFound("ROLE_ADMIN");
        createRoleIfNotFound("ROLE_USER");
    	if(userService.findOne(email) == null) {
	        try {
		        User u = new User();
		        u.setEmail(email);
		        u.setPassword(bcryptEncoder.encode(password));
		        u.setRole(roleAdmin);
		        u.setActive(true);
				userService.save(u);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    	}
       
    }
    
    @Transactional
    private Role createRoleIfNotFound (String name) {
        Role role = roleRepository.findByName(name);
        if (role == null) {
            role = new Role(name);
            role = roleRepository.save(role);
        }
        return role;
    }
}
