# Coffee-App

Coffee-App est une plateforme de gestion des machines à café. Réalisée dans le cadre d'un mini-projet JEE à l'université de Rouen Normandie, Master 2 GIL.

<ul>
<li><b>Executables/iot:</b> contient le jar de la machine à café (CoffeeIoT), le fichier de configuration et un script de lancement <b>coffeeIoT.bat</b> .</li>
<li><b>Executables/plateforme:</b> contient le jar de la plateforùe (CoffeeIoT) et un script de lancement <b>coffeeApp.bat</b>. <br/> Il contient également un fichier war déployable sur un serveur Tomcat.</li>
<ul>


<br/>
<h1>Pré-requis</h2>
<p>
Il faut configurer la base de données de la platefome (base de de donneés Mysql à nommer <b>coffee_bd</b> avec un utilisateur <b>admin</b>, password <b>coffee</b>).
</p>

<h2>Intégration d'une machine à café</h2>
<ul>
<li>Créer un compte utilisateur avec le compte admin (admin@gmail.com,P@sser1234).</li>
<li>Créer une machine en lui assignant comme utilisateur, l'utilisateur créé dans 2.</li>
<li>Récupérer l'id de la machine en question.</li>
<li>Modifier le fichier de configuration(conf.properties) de l'IoT (CoffeeIot) en mettant iot.machine le id de la machine (étape 4) et le port de la machine (iot.agentPort).</li>
<li>Changer dans le fichier de configuration (conf.properties) l'url et le port de la plateforme(localhost et 8080 par défaut).</li>
<li>Exécuter le fichier coffeeIoT.bat pour lancer la machine à café.</li>
</ul>

